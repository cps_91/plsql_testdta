# README #

### Overview of Code Base ###

* This repository contains a series of Oracle PL/SQL Scripts, Package, and Stored Procedures used to support core Central Planning Master Data Maintenance and Updates.  The Stored Procedures (script names prefixed with P5534 are executed via a custom JDE application P5500010.  Within P5500010, each Stored Procedure is configure with associated Parameters, and desired values.  When a Stored Procedure is selected and submitted, the custom program P5500010 connects with the Oracle Database for the current environment via a custom JDE BSSV B5500100, passes the Parameters, and executes the Stored Procedure.

### Structure of Code Base & Compile Guidelines ###

* The code is broken down into SQL script components. CPGlobal.sql should be executed first, followed by package PKCPMEMAIN.sql. The stored procedures (P5534001 through P5534006) can be executed in any order after the package.

* CPGlobal.sql: Grants, Objects, Types, and Sequences used in the Package PKCPMEMAIN and the individual Stored Procedures.  Please note: When generating this script, embedded section identifiers should be compiled separately in sequence.

* PKCPMEMAIN.sql: Main Package for the Stored Procedures.

* P5534001.sql: Total Purchase Lead-time Calculation per Branch/Item, and F4102.IBLTLV update.

* P5534002.sql: High Volume ABC Analysis per Branch, and F4102.IBABCS update.

* P5534003.sql: Safety Stock calculation using Sales Ledger and Purchase Receipt data, and F4102.IBSAFE update.

* P5534004.sql: Weekly spreading of Monthly Forecasts, and Insert/Update of F3460 records.

* P5534005.sql: Default setting of Branch/Item MRP Planning Code based on whether or not item had sales demand over a defined previous date range, and F4102.IBMPST update.

* P5534006.sql: Safety Stock calculation using Forecast Sales History and Purchase Receipt data, and F4102.IBSAFE update.

* P5534007.sql: Truncate workfile table F41UI003 used by the base JDE ABC Analysis program R4164.

### Requirements ###

* The Central Planning scripts require two custom tables that are managed via JDE OMW: 1) F5534CPM, used to capture runtime logging; and 2) F5534VAL, used by some of the update scripts to capture before and after changed values. 

### Contribution guidelines ###

* TESTDTA, CRPDTA, and PRDDTA are progressive sequences of repositories starting with TESTDTA where code is developed, refactored, unit tested, and regression tested in the JD Edwards E1 Development (DV) environment. TESTDTA is used throughout the scripts as a database schema.  
* Locally on a fat client, each local repository is stored in the folder structure C:\CPS_9.1\CP_CentralPlanning\PLSQL_TESTDTA, ..\PLSQL_CRPDTA, ..\PLSQL_PRODDTA respectively.
* Once approved, TESTDTA scripts are manually copied from their TESTDTA repository to the CRPDTA and PRODDTA repositories, and the TESTDTA database schema is refactored to CRPDTA and PRODDTA for each script respectively.

### Who to Contact ###

* Andy Collier is the original creator of the Repositories, and PL/SQL Code.  
* He may be reached at 434-825-5960.