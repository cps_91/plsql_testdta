/* Central Planning */
/* Truncate workfile table F41UI003 used by the base JDE ABC Analysis program R4164 */

CREATE OR REPLACE PROCEDURE p5534007 (vInParm IN number)
AS
BEGIN
IF vInParm = 0 THEN
pkCPMEMAIN.pProgramLog ('P5534007', '0', 'Start', '');
pkCPMEMAIN.pProgramLog ('P5534007', '0', 'Parameter used:'||vInParm, '');
EXECUTE IMMEDIATE 'TRUNCATE TABLE TESTDTA.F41UI003';
pkCPMEMAIN.pProgramLog ('P5534007', '0', 'F41UI003 Table Truncated','');
pkCPMEMAIN.pProgramLog ('P5534007', '0', 'End','');
ELSE
pkCPMEMAIN.pProgramLog ('P5534007', '0', 'Value OTHER THAN ZERO was used on IN parm.','');
raise_application_error(-20011,'Unknown Exception in P5534007 Procedure');
END IF;
EXCEPTION
WHEN OTHERS THEN
pkCPMEMAIN.pProgramLog ('P5534007', '1', 'OTHERS - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);
pkCPMEMAIN.pProgramLog ('P5534007', '1', 'OTHERS - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);
pkCPMEMAIN.pProgramLog ('P5534007', '1', 'OTHERS - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);
raise_application_error(-20011,'Unknown Exception in P5534007 Procedure');
END P5534007;
/

GRANT EXECUTE ON TESTDTA.p5534007 to bssvplsql;
