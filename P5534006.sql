/* Central Planning */
/* Calculate Safety Stock based on Forecast Sales History and Purchase Receipt Data */ 
/* and update F4102.IBSAFE */

CREATE OR REPLACE 
PROCEDURE p5534006(vDtFrmGre_in IN VARCHAR2, vDtToGre_in IN VARCHAR2, F4102MCUSEL_in IN VARCHAR2, F4102Sel_in IN VARCHAR2,
                   F4102EXCLPRP2_SAFE_in IN VARCHAR2, F3460Sel_in IN VARCHAR2, F43121Sel_in IN VARCHAR2, F4102ServLvl_in IN VARCHAR2,
                    F4102MPST_in IN VARCHAR2)
AS
    vDtFrmJul_v NATURAL := TO_CHAR(To_Date(vDtFrmGre_in, 'DD-MM-YY'),'YYYYDDD') - 1900000;
    vDtToJul_v NATURAL := TO_CHAR(To_Date(vDtToGre_in, 'DD-MM-YY'),'YYYYDDD') - 1900000;
    curlimit_v NUMBER := 1000;
    
    /* Section: Tables for storing parsed parameters */
     /* Parameter Selection for F4102 */
    tF4102SEL_v tF4102SEL;
    /* Parameter Selection for F3460 */
    tF3460Sel_v tF3460Sel;
    /* Parameter Selection for F43121 */
    tF43121SEL_v tF43121SEL;
     /* Parameter Selection for F4102 Active Branches*/
    tF4102MCUSEL_v tF4102MCUSEL;
     /* Parameter Selection for Excluding F4102 PRP2 Codes */
    tF4102EXCLPRP2_SAFE_v tF4102CATCODE3CHAR;
     /* Parameter Selection for ABC Service Level Percentages */
     tF4102ServLvl_v tF4102ServLvl;
     /* Parameter Selection for Including Planning Code */
     tF4102MPST_v tF4102MPST;

     /* Return Object from the Sales Quantity Demand Function */
    oF3460SlsHstDemand_v oF3460SlsHstDemand; 
    
    /* Return Object from the Purchase Receiver Lead-time Function */
    oF43121PurLT_v oF43121PurLT;

    /* Cursor for retrieving active Branch/Plants */
    CURSOR F4102_ActiveMCU_cur IS
      SELECT MCU 
      FROM TABLE(tF4102MCUSel_v);
 
    rF4102_ActiveMCU F4102_ActiveMCU_cur%ROWTYPE;  

   /* Cursor for selecting F4102 Records to Update */ 
    CURSOR F4102_cur(mcu_in VARCHAR2) IS
        SELECT mcu_in mcu, ib.IBITM itm, ib.IBLITM litm, ib.IBSTKT stkt,
             ib.IBLNTY lnty, ib.IBSAFE oldSAFE, ib.IBABCS abcs, ib.IBLTLV ltlv
       FROM F4102 ib
       JOIN (SELECT STKT, LNTY FROM TABLE(tF4102SEL_v)) sl
        ON ib.IBSTKT = sl.STKT
        AND ib.IBLNTY = sl.LNTY
       JOIN (SELECT MPST FROM TABLE(tF4102MPST_v)) p1
        ON p1.MPST = ib.IBMPST
       LEFT JOIN (SELECT CatCode3 FROM TABLE(tF4102EXCLPRP2_SAFE_v)) p2
        ON ib.IBPRP2 = p2.CatCode3
       WHERE ib.IBMCU = mcu_in
       AND p2.CatCode3 IS NULL;
      -- FOR UPDATE OF IBSAFE;

     TYPE tF4102 IS TABLE OF F4102_cur%ROWTYPE
      INDEX BY PLS_INTEGER;   
      
      tF4102_v tF4102;
 
      newSafe NUMBER;
      zscore_v NUMBER(6,4);
      parmStr CLOB;
      itm_v NUMBER;
      indx_v NUMBER;
  
BEGIN
    /* Log Parameters Used */
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Start', '');    
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Parameter 1: From Date',vDtFrmGre_in);    
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Parameter 2: To Date',vDtToGre_in);    
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Parameter 3: Item B/P MCU Inclusions',F4102MCUSEL_in);    
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Parameter 4: Item B/P STKT and LNTY Inclusions',F4102Sel_in);    
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Parameter 5: Item B/P PRP2 Exclusions',F4102EXCLPRP2_SAFE_in);    
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Parameter 6: Forecast Sales Inclusions',F3460Sel_in);    
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Parameter 7: Purchase Receipt Inclusions',F43121Sel_in);    
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Parameter 8: ABCS Rank Service Level',F4102ServLvl_in);    
    pkCPMEMAIN.pProgramLog ('P5534006', '0', 'Parameter 9: Item B/P MPST Inclusions',F4102MPST_in);    

    /* Populate F4102 Parameter Selection Criteria */
     tF4102SEL_v := pkCPMEMAIN.fF4102SEL(F4102Sel_in);
   /* Populate F3460 Parameter Selection Criteria */
     tF3460Sel_v := pkCPMEMAIN.fF3460Sel(F3460Sel_in);
   /* Populate F43121 Parameter Selection Criteria */
     tF43121SEL_v := pkCPMEMAIN.fF43121SEL(F43121Sel_in);
   /* Populate Active Branches Selection Criteria */
    tF4102MCUSEL_v := pkCPMEMAIN.fF4102MCUSEL(F4102MCUSEL_in);
   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
    tF4102EXCLPRP2_SAFE_v := pkCPMEMAIN.fF4102EXCLPRP2_SAFE(F4102EXCLPRP2_SAFE_in);    
   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
    tF4102ServLvl_v := pkCPMEMAIN.fF4102SERVLVL(F4102ServLvl_in);  
   /* Parse Selection Criteria for F42102 MPST to Include */
    tF4102MPST_v := pkCPMEMAIN.fF4102MPST(F4102MPST_in);    
    
    /* Active Item Branch/Plants */
    OPEN F4102_ActiveMCU_cur;
    LOOP
      FETCH F4102_ActiveMCU_cur INTO rF4102_ActiveMCU;
      EXIT WHEN F4102_ActiveMCU_cur%NOTFOUND; 
         OPEN F4102_cur(rF4102_ActiveMCU.mcu);
         LOOP
         FETCH F4102_cur BULK COLLECT INTO tF4102_v LIMIT curlimit_v;
           EXIT WHEN F4102_cur%NOTFOUND;
           EXIT WHEN tF4102_v.COUNT = 0;
               
           FOR indx IN 1 .. tF4102_v.COUNT
           LOOP
           indx_v := indx;
           itm_v := tF4102_v(indx).itm;

               /* Calculate  Sales Quantity Demand for Active Branch and Item */
               oF3460SlsHstDemand_v := pkCPMEMAIN.fF3460SlsHstDemand(vDtFrmJul_v, vDtToJul_v, tF3460Sel_v, rF4102_ActiveMCU.mcu, tF4102_v(indx).itm);

               /* Calculate Purchase Lead-times for Active Branch and Item */
               oF43121PURLT_v := pkCPMEMAIN.fF43121PURLT(vDtFrmJul_v, vDtToJul_v, tF43121SEL_v, rF4102_ActiveMCU.mcu, tF4102_v(indx).itm);

              /* If Purchase History is not found, then set the Mean to the Item B/P Leadtime Level and set Std Dev to 0 */
              If oF43121PURLT_v.PURLTMEAN = 0 Then 
                Begin
                  oF43121PURLT_v.PURLTMEAN := tF4102_v(indx).ltlv;
                  oF43121PURLT_v.PURLTSTDDEV := 1;
                End;
              End IF;

             IF oF3460SlsHstDemand_v.mcu != '*NONE' THEN
               Select NVL(z.ZSCORE,1)
               INTO zscore_v
               FROM TABLE(tF4102ServLvl_v) z WHERE z.ABCRANK = tF4102_v(indx).ABCS;
          
               /* Safety Stock = z(service level) * Sq Root ( (mean lt * std dev demand power 2) + (mean * std dev lt)demand power 2) */
               newSAFE := CASE WHEN oF3460SlsHstDemand_v.FQTMEAN = 0 OR oF43121PURLT_v.PURLTMEAN = 0 THEN 0
                           ELSE
                             ROUND( SQRT( (oF43121PURLT_v.PURLTMEAN * POWER(CASE WHEN oF3460SlsHstDemand_v.FQTSTDDEV != 0 THEN oF3460SlsHstDemand_v.FQTSTDDEV ELSE 1 END, 2) ) + 
                                ( POWER(oF3460SlsHstDemand_v.FQTMEAN,2) * POWER(CASE WHEN oF43121PURLT_v.PURLTSTDDEV != 0 THEN oF43121PURLT_v.PURLTSTDDEV ELSE 1 END,2) ) )
                                      * zscore_v)
                           END;           
               pkCPMEMAIN.pProgramLog ('P5534006', '9', 'After Safe Assignment', '');    
               UPDATE F4102 ib
                  SET ib.IBSAFE = newSAFE,
                      IBPID = 'P5534006',
                      IBUSER = 'PL/SQL',
                      IBUPMJ = pkCPMEMAIN.updateDateJul,
                      IBTDAY = TO_NUMBER(TO_CHAR(CURRENT_TIMESTAMP(6),'HH24MISS'))
                 WHERE IBMCU = rF4102_ActiveMCU.mcu
                 AND IBITM = tF4102_v(indx).itm;
                 COMMIT;

               pkCPMEMAIN.pLogValChg ('P5534006','F4102','IBSAFE',newSAFE,NULL,'IBMCU',NULL,tF4102_v(indx).mcu,'IBLITM',NULL,tF4102_v(indx).LITM,NULL,NULL,NULL, NULL,tF4102_v(indx).oldSAFE);
            ELSE
               pkCPMEMAIN.pProgramLog ('P5534006', '1', 'F3460 or F43121 Calculations are missing', 'Either oF3460SlsHstDemand_v.mcu or oF43121Purlt_v.muc = *None');    
            END IF;
          END LOOP; --indx
 
    END LOOP; --F4102_cur
    CLOSE F4102_cur;
    COMMIT; --Commit cannot be witin F4102_cur because of the UPDATE FOR
    
  END LOOP;
  CLOSE F4102_ActiveMCU_cur;

  COMMIT; 
  pkCPMEMAIN.pProgramLog ('P5534006', '0', 'End',''); 

   EXCEPTION
       WHEN ZERO_DIVIDE THEN
      	    CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'ZERO_DIVIDE - Item Branch', TO_CHAR(rF4102_ActiveMCU.mcu));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'ZERO_DIVIDE - Indx ' || To_Char(indx_v) || ' Item Nbr ', TO_CHAR(itm_v)); 
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'ZERO_DIVIDE - Data', 'F3460 | Mean: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTSTDDEV,'NULL')) || 'F43121 | Mean: ' || To_Char(NVL(oF43121PURLT_v.PURLTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF43121PURLT_v.PURLTSTDDEV,'NULL')));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'ZERO_DIVIDE - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'ZERO_DIVIDE - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'ZERO_DIVIDE - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN VALUE_ERROR THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'VALUE_ERROR - Item Branch', TO_CHAR(rF4102_ActiveMCU.mcu));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'VALUE_ERROR - Indx ' || To_Char(indx_v) || ' Item Nbr', TO_CHAR(itm_v)); 
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'VALUE_ERROR - Data', 'F3460 | Mean: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTSTDDEV,'NULL')) || 'F43121 | Mean: ' || To_Char(NVL(oF43121PURLT_v.PURLTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF43121PURLT_v.PURLTSTDDEV,'NULL')));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'VALUE_ERROR - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'VALUE_ERROR - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'VALUE_ERROR - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN COLLECTION_IS_NULL THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Item Branch', TO_CHAR(rF4102_ActiveMCU.mcu));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Indx ' || To_Char(indx_v) || ' Item Nbr', TO_CHAR(itm_v)); 
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Data', 'F3460 | Mean: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTSTDDEV,'NULL')) || 'F43121 | Mean: ' || To_Char(NVL(oF43121PURLT_v.PURLTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF43121PURLT_v.PURLTSTDDEV,'NULL')));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN TOO_MANY_ROWS THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Item Branch', TO_CHAR(rF4102_ActiveMCU.mcu));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Indx ' || To_Char(indx_v) || ' Item Nbr', TO_CHAR(itm_v)); 
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Data', 'F3460 | Mean: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTSTDDEV,'NULL')) || 'F43121 | Mean: ' || To_Char(NVL(oF43121PURLT_v.PURLTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF43121PURLT_v.PURLTSTDDEV,'NULL')));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'COLLECTION_IS_NULL - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN NO_DATA_FOUND THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'NO_DATA_FOUND - Item Branch', TO_CHAR(rF4102_ActiveMCU.mcu));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'NO_DATA_FOUND - Item Info', 'Indx: ' || To_Char(indx_v) || ' Item Nbr: ' || TO_CHAR(itm_v)); 
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'NO_DATA_FOUND - Data', 'F3460 Branch: ' || oF3460SlsHstDemand_v.mcu || ' F43121 Branch: ' || oF43121PURLT_v.mcu);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'NO_DATA_FOUND - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'NO_DATA_FOUND - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'NO_DATA_FOUND - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN OTHERS THEN
      	    CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'OTHERS - Item Branch', TO_CHAR(rF4102_ActiveMCU.mcu));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'OTHERS - Indx ' || To_Char(indx_v) || ' Item Nbr', TO_CHAR(itm_v)); 
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'OTHERS - Data', 'F42199 | Mean: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF3460SlsHstDemand_v.FQTSTDDEV,'NULL')) || 'F43121 | Mean: ' || To_Char(NVL(oF43121PURLT_v.PURLTMEAN,'NULL')) || ' Std Dev: ' || To_Char(NVL(oF43121PURLT_v.PURLTSTDDEV,'NULL')));    
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'OTHERS - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'OTHERS - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534006', '1', 'OTHERS - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
            raise_application_error(-20011,'Unknown Exception in P5534006 Procedure'); 
END P5534006;
/
GRANT EXECUTE ON TESTDTA.p5534006 to bssvplsql;