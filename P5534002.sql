/* Central Planning */
/* Calculate High Volume ABC Ranking and Update F4102.IBABCS */ 

CREATE OR REPLACE 
PROCEDURE P5534002(vDtFrmGre_in IN VARCHAR2, vDtToGre_in IN VARCHAR2, F4102MCUSEL_in IN VARCHAR2,F4102Sel_in IN VARCHAR2,
                   F4102EXCLPRP2_ABCS_in IN VARCHAR2, F42199Sel_in IN VARCHAR2)
AS
    vDtFrmJul_v NATURAL := TO_CHAR(To_Date(vDtFrmGre_in, 'DD-MM-YY'),'YYYYDDD') - 1900000;
    vDtToJul_v NATURAL := TO_CHAR(To_Date(vDtToGre_in, 'DD-MM-YY'),'YYYYDDD') - 1900000;
    
    curlimit_v NUMBER := 1000;
    
    /* Section: Tables for storing parsed parameters */
     /* Parameter Selection for F4102 */
    tF4102SEL_v tF4102SEL;
    /* Parameter Selection for F4102 */
    tF42199SEL_v tF42199SEL;
     /* Parameter Selection for F4102 Active Branches*/
    tF4102MCUSEL_v tF4102MCUSEL;
     /* Parameter Selection for Excluding F4102 PRP2 Codes */
    tF4102EXCLPRP2_ABCS_v tF4102CATCODE3CHAR;
     /* Parameter Selection for F41001 ABC Sales Percentages */
    tF41001ABCSPerc_v tF41001ABCSPerc;
     /* Return Table from the Cumulative Distribution Function */
    tF42199CumeDist_v tF42199CumeDist;

    /* Cursor for retrieving active Branch/Plants */
    CURSOR F4102_ActiveMCU_cur IS
      SELECT MCU 
      FROM TABLE(tF4102MCUSel_v);
 
    rF4102_ActiveMCU F4102_ActiveMCU_cur%ROWTYPE;  

   /* Cursor for selecting F4102 Records to Update */ 
    CURSOR F4102_cur(mcu_in VARCHAR2, tF42199CumeDist_in tF42199CumeDist) IS
        SELECT mcu_in mcu, ib.IBITM itm, ib.IBLITM litm, ib.IBSTKT stkt,
             ib.IBLNTY lnty, ib.IBABCS oldABCS, cd.UORGCUMEDIST UORGCUMEDIST
       FROM F4102 ib
       JOIN (SELECT STKT, LNTY FROM TABLE(tF4102SEL_v)) sl
        ON ib.IBSTKT = sl.STKT
        AND ib.IBLNTY = sl.LNTY
       LEFT JOIN (SELECT CatCode3 FROM TABLE(tF4102EXCLPRP2_ABCS_v)) p2
        ON ib.IBPRP2 = p2.CatCode3
       JOIN (SELECT * FROM TABLE(tF42199CumeDist_in)) cd
        ON IBMCU = cd.mcu
        AND IBLITM = cd.litm
       WHERE ib.IBMCU = mcu_in
       AND p2.CatCode3 IS NULL;
     --  FOR UPDATE OF IBABCS;

     TYPE tF4102 IS TABLE OF F4102_cur%ROWTYPE
      INDEX BY PLS_INTEGER;   
      
      tF4102_v tF4102;
    /* Variable for storing ABC Rank assigned to specific Branch/Plant
        And Item */
    abcRank_v VARCHAR2(1);
    
    /* Object for temporary storage of Branch/Plant Constants ABC
       Sales Percentages used for comparing calculated 
       Cumulative distributions to assign ABC Ranks to Branch/Plant
       Items */
    oF41001ABCSPerc_v oF41001ABCSPerc;
    newABCS VARCHAR(1);
  
BEGIN
    pkCPMEMAIN.pProgramLog ('P5534002', '0', 'Start','Calculate Cumulative Distribution of Sales Quantity Demand for High Volume items, and update the Item B/P ABC Sales Dollars Rank field F4102.IBABCS.');    
    pkCPMEMAIN.pProgramLog ('P5534002', '0', 'Parameter 1: From Date',vDtFrmGre_in);    
    pkCPMEMAIN.pProgramLog ('P5534002', '0', 'Parameter 2: To Date',vDtToGre_in);    
    pkCPMEMAIN.pProgramLog ('P5534002', '0', 'Parameter 3: Item B/P MCU Inclusions',F4102MCUSEL_in);    
    pkCPMEMAIN.pProgramLog ('P5534002', '0', 'Parameter 4: Item B/P STKT and LNTY Inclusions',F4102Sel_in);    
    pkCPMEMAIN.pProgramLog ('P5534002', '0', 'Parameter 5: Item B/P PRP2 Exclusions',F4102EXCLPRP2_ABCS_in);    
    pkCPMEMAIN.pProgramLog ('P5534002', '0', 'Parameter 6: Item B/P PRP2 Exclusions',F42199Sel_in);    
    
    /* Populate F4102 Parameter Selection Criteria */
     tF4102SEL_v := pkCPMEMAIN.FF4102SEL(F4102Sel_in);
    /* Populate F4102 Parameter Selection Criteria */
     tF42199SEL_v := pkCPMEMAIN.FF42199SEL(F42199Sel_in);
    /* Populate Active Branches Selection Criteria */
    tF4102MCUSEL_v := pkCPMEMAIN.FF4102MCUSEL(F4102MCUSEL_in);
    /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
    tF4102EXCLPRP2_ABCS_v := pkCPMEMAIN.FF4102EXCLPRP2_ABCS(F4102EXCLPRP2_ABCS_in);    

   
    /* Active Item Branch/Plants */
    OPEN F4102_ActiveMCU_cur;
    LOOP
      FETCH F4102_ActiveMCU_cur INTO rF4102_ActiveMCU;
      EXIT WHEN F4102_ActiveMCU_cur%NOTFOUND; 
      /* Parse Selection Criteria for F41001 ABC Sales Percentages */
      tF41001ABCSPerc_v := pkCPMEMAIN.FF41001ABCSPerc(rF4102_ActiveMCU.mcu);   
      /* Calculate Cumulative Sale Quantity Demand for Active Branch and Items */
      tF42199CumeDist_v := pkCPMEMAIN.FF42199CumeDist(vDtFrmJul_v, vDtToJul_v, rF4102_ActiveMCU.mcu, tF42199SEL_v);
      OPEN F4102_cur(rF4102_ActiveMCU.mcu, tF42199CumeDist_v);
      LOOP
      FETCH F4102_cur BULK COLLECT INTO tF4102_v LIMIT curlimit_v;
        EXIT WHEN F4102_cur%NOTFOUND;
        EXIT WHEN tF4102_v.COUNT = 0;
        FOR indx IN 1 .. tF4102_v.COUNT
        LOOP
              newABCS :=  CASE 
                        WHEN tF4102_v(indx).uorgcumedist <= tF41001ABCSPerc_v(1).CIIA1 Then 'A'
                        WHEN tF4102_v(indx).uorgcumedist <= tF41001ABCSPerc_v(1).CIIA2 Then 'B'
                        WHEN tF4102_v(indx).uorgcumedist <= tF41001ABCSPerc_v(1).CIIA3 Then 'C'
                        ELSE ' ' End;
              UPDATE F4102 ib
                SET ib.IBABCS = newABCS,
                    IBPID = 'P5534002',
                    IBUSER = 'PL/SQL',
                    IBUPMJ = pkCPMEMAIN.updateDateJul,
                    IBTDAY = TO_NUMBER(TO_CHAR(CURRENT_TIMESTAMP(6),'HH24MISS'))
                    WHERE IBMCU = tF4102_v(indx).mcu
                    AND IBLITM = tF4102_v(indx).LITM;
                 --WHERE CURRENT OF F4102_cur;  
               pkCPMEMAIN.pLogValChg ('P5534002','F4102','IBABCS',NULL,newABCS,'IBMCU',NULL,tF4102_v(indx).mcu,'IBLITM',NULL,tF4102_v(indx).LITM,NULL,NULL,NULL, tF4102_v(indx).oldABCS, NULL);
          END LOOP; --indx
      END LOOP; --F4102_cur
      CLOSE F4102_cur;
      COMMIT; --Commit cannot be witin F4102_cur because of the UPDATE FOR  4/11/2017 UPDATE REMOVED FROM CURSOR
  END LOOP;
  CLOSE F4102_ActiveMCU_cur;
  COMMIT; 
  pkCPMEMAIN.pProgramLog ('P5534002', '0', 'End','');   
  EXCEPTION
       WHEN ZERO_DIVIDE THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN VALUE_ERROR THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN SUBSCRIPT_BEYOND_COUNT THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN SUBSCRIPT_OUTSIDE_LIMIT THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN COLLECTION_IS_NULL THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN TOO_MANY_ROWS THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN NO_DATA_FOUND THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN OTHERS THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534002', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
 
END P5534002;
/
GRANT EXECUTE ON TESTDTA.p5534002 to bssvplsql;