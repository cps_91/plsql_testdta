/* Central Planning  */
/* Set Default Value for Planning Code and Update F4102.IBMPST */

CREATE OR REPLACE 
PROCEDURE P5534005(F4102MCUSEL_in IN VARCHAR2, F4102Sel_in IN VARCHAR2, F4102EXCLPRP2_PLANCODE_in IN VARCHAR2, F4102MPST_in IN VARCHAR2)
AS
    curlimit_v NUMBER := 1000;
    
    /* Section: Tables for storing parsed parameters */
    /* Parameter Selection for F4102 */
    tF4102SEL_v tF4102SEL;
    /* Parameter Selection for F4102 Active Branches*/
    tF4102MCUSEL_v tF4102MCUSEL;
    /* Parameter Selection for Excluding F4102 PRP2 Codes */
    tF4102EXCLPRP2_PLANCODE_v tF4102CATCODE3CHAR;

    /* Cursor for retrieving active Branch/Plants */
    CURSOR F4102_ActiveMCU_cur IS
      SELECT MCU 
      FROM TABLE(tF4102MCUSel_v);
 
    rF4102_ActiveMCU F4102_ActiveMCU_cur%ROWTYPE;  

    frmYr_v NUMBER := To_NUMBER(To_CHAR(add_months(sysdate, -24),'YY'));
    toYr_v NUMBER :=  To_NUMBER(To_CHAR(sysdate,'YY'));
    
   /* Cursor for selecting F4102 Records to Update Plan Code (IBMPST) */ 
    CURSOR F4102_cur(mcu_in VARCHAR2) IS
        SELECT mcu_in mcu, ib.IBITM itm, ib.IBLITM litm, ib.IBSTKT stkt,
             ib.IBLNTY lnty, ib.IBMPST mpst
       FROM F4102 ib
       JOIN (SELECT STKT, LNTY FROM TABLE(tF4102SEL_v)) sl
        ON ib.IBSTKT = sl.STKT
        AND Ib.IBLNTY = sl.LNTY
       LEFT JOIN (SELECT CatCode3 FROM TABLE(tF4102EXCLPRP2_PLANCODE_v)) p2
        ON ib.IBPRP2 = p2.CatCode3
       WHERE ib.IBMCU = mcu_in
       AND p2.CatCode3 IS NULL
       AND (SELECT COUNT(*)
            FROM f4115 h
            WHERE h.ihfy BETWEEN frmYR_v AND toYR_v
            AND h.ihcms != 0
            AND h.IHMCU = ib.IBMCU
            AND h.IHITM = ib.IBITM) > 0;

     TYPE tF4102_02 IS TABLE OF F4102_cur%ROWTYPE
      INDEX BY PLS_INTEGER;   
      tF4102_v tF4102_02;
 
      parmStr CLOB;
 
BEGIN
    /* Log Parameters Used */
    pkCPMEMAIN.pProgramLog ('P5534005', '0', 'Start', '');    
    pkCPMEMAIN.pProgramLog ('P5534005', '0', 'Parameter 1: Item B/P MCU Inclusions',F4102MCUSEL_in);    
    pkCPMEMAIN.pProgramLog ('P5534005', '0', 'Parameter 2: Item B/P STKT and LNTY Inclusions',F4102Sel_in);    
    pkCPMEMAIN.pProgramLog ('P5534005', '0', 'Parameter 3: Item B/P PRP2 Exclusions',F4102EXCLPRP2_PLANCODE_in);    
    pkCPMEMAIN.pProgramLog ('P5534005', '0', 'Parameter 4: Item B/P MRP Planning Code MPST',F4102MPST_in);    

    /* Populate F4102 Parameter Selection Criteria */
    tF4102SEL_v := pkCPMEMAIN.FF4102SEL(F4102Sel_in);
     
     /* Populate Active Branches Selection Criteria */
    tF4102MCUSEL_v := pkCPMEMAIN.FF4102MCUSEL(F4102MCUSEL_in);
   
   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
    tF4102EXCLPRP2_PLANCODE_v := pkCPMEMAIN.FF4102EXCLPRP2_FORECAST(F4102EXCLPRP2_PLANCODE_in);    
 
   /* Active Item Branch/Plants */
    OPEN F4102_ActiveMCU_cur;
    LOOP
      FETCH F4102_ActiveMCU_cur INTO rF4102_ActiveMCU;
      EXIT WHEN F4102_ActiveMCU_cur%NOTFOUND; 

        /* Set planning code to passed in value for active records
           that are not excluded */
        OPEN F4102_cur(rF4102_ActiveMCU.mcu);
        LOOP
        FETCH F4102_cur BULK COLLECT INTO tF4102_v LIMIT curlimit_v;
          -- EXIT WHEN F4102_cur%NOTFOUND; 
           EXIT WHEN tF4102_v.COUNT = 0;
               
           FOR indx IN 1 .. tF4102_v.COUNT
           LOOP
                  
                   UPDATE F4102 ib 
                    SET ib.IBMPST = F4102MPST_in,
                        ib.IBUSER = 'PL/SQL',
                        ib.IBUPMJ = pkCPMEMAIN.updatedateJul,
                        ib.IBTDAY = TO_NUMBER(TO_CHAR(CURRENT_TIMESTAMP(6),'HH24MISS'))
                    WHERE ib.IBMCU = rF4102_ActiveMCU.mcu
                    AND ib.IBITM = tF4102_v(indx).itm;
 
        
          END LOOP; --indx
          COMMIT;
          
       END LOOP; --F4102_cur
       CLOSE F4102_cur;
       COMMIT; 
    
  END LOOP;
  CLOSE F4102_ActiveMCU_cur;

  COMMIT; 
  pkCPMEMAIN.pProgramLog ('P5534005', '0', 'End',''); 

   EXCEPTION
       WHEN ZERO_DIVIDE THEN
      	    CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
            WHEN DUP_VAL_ON_INDEX THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN VALUE_ERROR THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN SUBSCRIPT_BEYOND_COUNT THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN SUBSCRIPT_OUTSIDE_LIMIT THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN COLLECTION_IS_NULL THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN TOO_MANY_ROWS THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN NO_DATA_FOUND THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN OTHERS THEN
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534005', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
            raise_application_error(-20011,'Unknown Exception in P5534005 Procedure'); 
END P5534005;
/
GRANT EXECUTE ON TESTDTA.p5534005 to bssvplsql;