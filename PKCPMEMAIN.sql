--------------------------------------------------------
--  DDL for Package PKCPMEMAIN
--------------------------------------------------------
CREATE OR REPLACE PACKAGE PKCPMEMAIN 
AS
  /* Current System Date converted to JDE Julian Date.  Used for
     Recording current date in JDE Table Update fields */ 
  updateDateJul NUMBER (6,0) := TO_NUMBER(TO_CHAR(sysdate,'YYYYDDD')) - 1900000;
  
  /* Used for retrieving Branch/Plant Constants ABC Sales Percentages */                                     
  FUNCTION FF41001ABCSPerc(mcu_in IN VARCHAR2) RETURN tF41001ABCSPerc;
  
  /* Used for calculating the Cumulative Distribution of total Sales Order Demand by Item Branch/Plant and Short Code given a Date Range against the selected Sales Ledger Sales Order Detail records */
  FUNCTION FF42199CumeDist (datefromjul_in IN NUMBER, datetojul_in IN NUMBER,mcu_in IN VARCHAR2, 
                            tF42199SEL_v tF42199SEL) RETURN tF42199CumeDist;

  /* Used for calculating the Standard Deviation and Mean of total Sales Order Demand by Item Branch/Plant and Short Code given a Date Range against the selected Sales Ledger Sales Order Detail records */
  FUNCTION FF42199DEMAND (datefromjul_in IN NUMBER, datetojul_in IN NUMBER, tF42199SEL_in tF42199SEL, mcu_in IN VARCHAR2, itm_in IN NUMBER) RETURN oF42199DEMAND;

  /* Used for calculating the Standard Deviation and Mean of Purchasing Lead-times by Item Branch/Plant and Short Code given a Date Range against the selected Purchase Order Receiver records*/
  FUNCTION FF43121PURLT (datefromjul_in IN NUMBER, datetojul_in IN NUMBER, tF43121SEL_in tF43121SEL, mcu_in IN VARCHAR2, itm_in IN NUMBER) RETURN oF43121PURLT;

   /* Calculate Total Purchase Lead-time */
   FUNCTION FF43090TotPurLT(mcu_in VARCHAR, itm_in IN NUMBER, LTPrepare_in IN NUMBER, LTReceive_in IN NUMBER) RETURN NUMBER;
  
   /* Parse Parameter String */
--   FUNCTION fParseParameter (parm_v IN VARCHAR2, delimeter_v IN VARCHAR2) RETURN tParsedStr;
   
   /* Parse Selection Criteria for F42199 */
   FUNCTION FF42199SEL(parmstring_in VARCHAR2) RETURN tF42199Sel;

  /* Parse Selection Criteria for F43121 */
   FUNCTION FF43121SEL(parmstring_in VARCHAR2) RETURN tF43121Sel;

   /* Parse Selection Criteria for F42102 */
   FUNCTION FF4102SEL(parmstring_in VARCHAR2) RETURN tF4102Sel;
   
   /* Parse Selection Criteria for F42102 Active Branches */
   FUNCTION FF4102MCUSEL(parmstring_in VARCHAR2) RETURN tF4102MCUSel;
   
   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
   FUNCTION FF4102EXCLPRP2_LTLV(parmstring_in VARCHAR2) RETURN tF4102CatCode3Char;

   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
   FUNCTION FF4102EXCLPRP2_ABCS(parmstring_in VARCHAR2) RETURN tF4102CatCode3Char;    
   
   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
   FUNCTION FF4102EXCLPRP2_SAFE(parmstring_in VARCHAR2) RETURN tF4102CatCode3Char;    
   
   /* Parse Selection Criteria for F42102 ABCS Rank Service Level */
   FUNCTION FF4102SERVLVL(parmstring_in VARCHAR2) RETURN tF4102SERVLVL;    
   
   /* Given JDE Julian Date Range, return Days DiFFerence */
   FUNCTION fDtJulDiFFDys(DtJulFrm NUMBER, DtJulTo NUMBER) RETURN NUMBER;
   
   /* Parse Exlusion Criteria for Foreacts based on Item B/P PRP2 field */
   FUNCTION FF4102EXCLPRP2_FORECAST(parmstring_in VARCHAR2) RETURN tF4102CatCode3Char;
   
   /* Parse Selection Criteria for Monthly Foreacts for Forecast Spreading */
   FUNCTION FF3460MTHFCSEL(parmstring_in VARCHAR2) RETURN tF3460MTHFCSEL;

   /* Parse Forecast Monthly to Weekly Spreading Percentages */
   FUNCTION FF3460SPREADPRCNT(parmstring_in VARCHAR2) RETURN tF3460SPREADPRCNT;

   /* Parse Forecast Type to use for Weekly Forecasts */
   FUNCTION FF3460WKFCSEL(parmstring_in VARCHAR2) RETURN tF3460WKFCSEL;

   /* Parse Inclusion Criteria for Item B/P Planning Code MPST  */
   FUNCTION FF4102MPST(parmstring_in VARCHAR2) RETURN tF4102MPST;

  /* Parse Parameter String */
   FUNCTION fParseParameter (str_in IN VARCHAR2,delimiter_in IN VARCHAR2) RETURN tParsedStr;  
   
   /* Program Logging Procedure */
   PROCEDURE pProgramLog (PROGRAM_in VARCHAR2, error_in VARCHAR2, message_in VARCHAR2, description_in CLOB);
   
   /* Value Change Logging */
   PROCEDURE pLogValChg (PROGRAM_in IN VARCHAR2, TABLE_NAME_in IN VARCHAR2, CHG_FIELD_in IN VARCHAR2, CHG_NUM_VALUE_in	IN NUMBER, CHG_CHAR_VALUE_in IN VARCHAR2, KEY1_FIELD_in	IN VARCHAR2, KEY1_NUM_VALUE_in IN NUMBER, KEY1_CHAR_VALUE_in IN VARCHAR2, KEY2_FIELD_in IN VARCHAR2, KEY2_NUM_VALUE_in	IN NUMBER, KEY2_CHAR_VALUE_in IN VARCHAR2, KEY3_FIELD_in IN VARCHAR2, KEY3_NUM_VALUE_in IN NUMBER, KEY3_CHAR_VALUE_in IN VARCHAR2, OLD_CHAR_VALUE_in IN VARCHAR2, OLD_NUM_VALUE_in IN NUMBER);
  
   /* Parse Selection Criteria for F3460 */
   FUNCTION fF3460Sel(parmstring_in VARCHAR2) RETURN tF3460Sel;

   /* Used for calculating the Standard Deviation and Mean of total Sales Order Demand that resides in the 
   F3460 Forecast Table by Item Branch/Plant and Short Code given a Date Range*/
   FUNCTION fF3460SlsHstDemand(datefromjul_in IN NUMBER, datetojul_in IN NUMBER, tF3460Sel_in IN tF3460Sel, mcu_in IN VARCHAR2, itm_in IN NUMBER) RETURN oF3460SlsHstDemand;

END pkCPMEMAIN;
/

CREATE OR REPLACE PACKAGE BODY PKCPMEMAIN AS 

 /* Retrieve ABC Sales Rank Percentages */
  FUNCTION FF41001ABCSPerc(mcu_in IN VARCHAR2)
    RETURN tF41001ABCSPerc 
  AS 
     /* ABC Sales Percentages */
      tF41001ABCSPerc_v tF41001ABCSPerc; 
  BEGIN
     SELECT oF41001ABCSPerc(ci.CIMCU, ci.CIIA1, ci.CIIA2, ci.CIIA3)
     BULK COLLECT INTO tF41001ABCSPerc_v
     FROM F41001 ci
     WHERE ci.CIMCU = mcu_in;
    RETURN tF41001ABCSPerc_v;
  END FF41001ABCSPerc;


  /* Calculate the Sales Ledger Cumulative Distribution for Order Quantity */
   FUNCTION FF42199CumeDist (datefromjul_in IN NUMBER, datetojul_in IN NUMBER, mcu_in IN VARCHAR2, tF42199SEL_v tF42199SEL)
	 RETURN tF42199CumeDist
   AS
	/* Cumulative Distributions of Total Sales Demand by Branch/Plant and Item */
	  tF42199CumeDist_v tF42199CumeDist;
	BEGIN
	  WITH salesHistQtyPerc AS
	  (SELECT sl.SLMCU, sl.SLLITM, RATIO_TO_REPORT(sl.SLUORG) OVER (PARTITION BY sl.SLMCU) QtyPerc --Generates Percentage of Total Quantity for Each Item Branch
	  FROM F42199 sl
	  JOIN 
	  (SELECT DCTO, LNTY, LTTR FROM TABLE(tF42199SEL_v)) sel
	  ON sl.SLDCTO = sel.DCTO
	  AND sl.SLLNTY = sel.LNTY
	  AND sl.SLLTTR = sel.LTTR
	  WHERE sl.SLMCU = mcu_in
	  AND sl.SLDRQJ BETWEEN datefromjul_in AND datetojul_in
	  GROUP BY sl.SLMCU, sl.SLLITM)
      
	  SELECT oF42199CumeDist(sl.SLMCU, sl.SLLITM,
	   CUME_DIST() Over(Order by SUM(QtyPerc) DESC), 
	   SUM(QtyPerc) OVER (PARTITION BY sl.SLMCU ORDER BY QtyPerc)) UORGCUMEDIST --Generate Cumulative Percentage used for assigning ABC Ranking
	  BULK COLLECT INTO tF42199CumeDist_v
	  FROM salesHistQtyPerc sl;
     RETURN tF42199CumeDist_v;
	END FF42199CumeDist; 

  /* Used for calculating the Standard Deviation and Mean of total Sales Order Demand by Item Branch/Plant and Short Code given a Date Range against the selected Sales Ledger Sales Order      Detail records */
  FUNCTION FF42199DEMAND (datefromjul_in IN NUMBER, datetojul_in IN NUMBER, tF42199SEL_in tF42199SEL, mcu_in IN VARCHAR2, itm_in IN NUMBER)
  RETURN oF42199DEMAND
  AS
      /* Cumulative Distributions of Total Sales Demand by
          Branch/Plant and Item */
    oF42199DEMAND_v oF42199DEMAND; 
    parmStr VARCHAR2(1000);
    BEGIN
     SELECT oF42199DEMAND(sl.SLMCU, sl.SLLITM, 
          STDDEV_POP(sl.SLUORG), AVG(sl.SLUORG)) 
            INTO oF42199DEMAND_v      
            FROM  F42199 sl
            JOIN (SELECT DCTO, LNTY, LTTR FROM TABLE(tF42199SEL_in)) sel
              ON sl.SLDCTO = sel.DCTO
              AND sl.SLLNTY = sel.LNTY
              AND sl.SLLTTR = sel.LTTR
            WHERE sl.SLMCU = mcu_in 
            AND sl.SLITM = itm_in
            AND sl.SLUORG > 0
            AND sl.SLDRQJ BETWEEN datefromjul_in AND datetojul_in
            GROUP BY sl.SLMCU, sl.SLLITM;            
      CASE
        WHEN of42199DEMAND_v.UORGSTDDEV IS NULL THEN of42199DEMAND_v.UORGSTDDEV := 0;
        WHEN of42199DEMAND_v.UORGMEAN IS NULL THEN of42199DEMAND_v.UORGMEAN := 0;
        ELSE
          RETURN oF42199DEMAND_v;
        END CASE;
      RETURN oF42199DEMAND_v;
       EXCEPTION
          WHEN ZERO_DIVIDE THEN
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF42199SEL_in);
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'ZERO_DIVIDE - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'ZERO_DIVIDE - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'ZERO_DIVIDE - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN VALUE_ERROR THEN
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF42199SEL_in);
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'VALUE_ERROR - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'VALUE_ERROR - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'VALUE_ERROR - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
          WHEN COLLECTION_IS_NULL THEN
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF42199SEL_in);
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'COLLECTION_IS_NULL - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'COLLECTION_IS_NULL - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'COLLECTION_IS_NULL - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN TOO_MANY_ROWS THEN
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF42199SEL_in);
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'TOO_MANY_ROWS - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'TOO_MANY_ROWS - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'TOO_MANY_ROWS - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN NO_DATA_FOUND THEN
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF42199SEL_in);
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pProgramLog ('FF42199DEMAND', '1', 'Returned Branch', of42199DEMAND_v.mcu);    
               pProgramLog ('FF42199DEMAND', '1', 'Returned Item Nbr', of42199DEMAND_v.litm);    
               pProgramLog ('FF42199DEMAND', '1', 'Returned STDDEV_POP', TO_CHAR(of42199DEMAND_v.UORGSTDDEV));    
               pProgramLog ('FF42199DEMAND', '1', 'Returned AVG', TO_CHAR(of42199DEMAND_v.UORGMEAN));    
                of42199DEMAND_v := of42199DEMAND('*NONE',' ',0,0);
               pProgramLog ('FF42199DEMAND', '1', 'Returned Branch', of42199DEMAND_v.mcu);    
               pProgramLog ('FF42199DEMAND', '1', 'Returned Item Nbr', TO_CHAR(of42199DEMAND_v.litm));    
               pProgramLog ('FF42199DEMAND', '1', 'Returned STDDEV_POP', TO_CHAR(of42199DEMAND_v.UORGSTDDEV));    
               pProgramLog ('FF42199DEMAND', '1', 'Returned AVG', TO_CHAR(of42199DEMAND_v.UORGMEAN));    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'NO_DATA_FOUND - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'NO_DATA_FOUND - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'NO_DATA_FOUND - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
               RETURN oF42199DEMAND_v;
          WHEN OTHERS THEN
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF42199SEL_in);
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'OTHERS - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'OTHERS - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF42199DEMAND', '1', 'OTHERS - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
               raise_application_error(-20011,'Unknown Exception in FF42199DEMAND Procedure');     
  END FF42199DEMAND;  

  /* Used for calculating the Standard Deviation and Mean of Purchasing Lead-times by Item Branch/Plant and Short Code given a Date Range against the selected Purchase Order Receiver      records*/
  FUNCTION FF43121PURLT (datefromjul_in IN NUMBER, datetojul_in IN NUMBER, tF43121SEL_in tF43121SEL, mcu_in IN VARCHAR2, itm_in IN NUMBER) 
  RETURN oF43121PURLT
  AS
   parmStr VARCHAR2(1000);
   oF43121PURLT_v oF43121PURLT; 
  BEGIN
     SELECT oF43121PURLT(pr.PRMCU, pr.PRLITM, 
          STDDEV_POP(pkcpmemain.fDtJulDiFFDys(pr.PRTRDJ,pr.PRRCDJ)), AVG(pkcpmemain.fDtJulDiFFDys(pr.PRTRDJ,pr.PRRCDJ))) -- TRDJ: Transaction Date, RCDJ: Receipt Date
            INTO oF43121PURLT_v      
            FROM  F43121 pr
            JOIN (SELECT DCTO, LNTY, LTTR FROM TABLE(tF43121SEL_in)) sel
              ON pr.PRDCTO = sel.DCTO
              AND pr.PRLNTY = sel.LNTY
              AND pr.PRLTTR = sel.LTTR
            WHERE pr.PRMCU = mcu_in
            AND pr.PRITM = itm_in
            AND pr.PRMATC = 1 
            AND pr.PRUREC > 0   --Units Recieved
            AND pr.PRRCDJ BETWEEN datefromjul_in AND datetojul_in --Receipt Date
            GROUP BY pr.PRMCU, pr.PRLITM;
 
      CASE
        WHEN oF43121PURLT_v.PURLTSTDDEV IS NULL THEN oF43121PURLT_v.PURLTSTDDEV := 0;
        WHEN oF43121PURLT_v.PURLTMEAN IS NULL THEN oF43121PURLT_v.PURLTMEAN := 0;
        ELSE       
         RETURN oF43121PURLT_v;
        END CASE;
       RETURN oF43121PURLT_v;
       EXCEPTION
          WHEN ZERO_DIVIDE THEN
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF43121SEL_in);
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN VALUE_ERROR THEN
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF43121SEL_in);
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: F42199 Sel Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: Item Nbr', TO_CHAR(itm_in));    
               pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: STDDEV_POP', TO_CHAR(NVL(oF43121PURLT_v.PURLTSTDDEV,'NULL')));    
               pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: AVG', TO_CHAR(NVL(oF43121PURLT_v.PURLTMEAN,'NULL')));    
               oF43121PURLT_v := oF43121PURLT('*NONE',' ',0,0);
               pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: Branch', oF43121PURLT_v.mcu);    
               pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: Item Nbr', oF43121PURLT_v.litm);    
               pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: STDDEV_POP', TO_CHAR(oF43121PURLT_v.PURLTSTDDEV));    
               pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: AVG', TO_CHAR(oF43121PURLT_v.PURLTMEAN));    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'VALUE ERROR: Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN SUBSCRIPT_BEYOND_COUNT THEN
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF43121SEL_in);
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN SUBSCRIPT_OUTSIDE_LIMIT THEN
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF43121SEL_in);
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN COLLECTION_IS_NULL THEN
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF43121SEL_in);
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN TOO_MANY_ROWS THEN
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF43121SEL_in);
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN NO_DATA_FOUND THEN
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'NO DATA: Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF43121SEL_in);
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'NO DATA: F42199 Sel Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'NO DATA: Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'NO DATA: Item Nbr', TO_CHAR(itm_in));    
               pProgramLog ('FF43121PURLT', '1', 'NO DATA: Branch', oF43121PURLT_v.mcu);    
               pProgramLog ('FF43121PURLT', '1', 'NO DATA: Item Nbr', oF43121PURLT_v.litm);    
               pProgramLog ('FF43121PURLT', '1', 'NO DATA: STDDEV_POP', NVL(TO_CHAR(oF43121PURLT_v.PURLTSTDDEV),'NULL'));    
               pProgramLog ('FF43121PURLT', '1', 'NO DATA: AVG', NVL(TO_CHAR(oF43121PURLT_v.PURLTMEAN),'NULL'));    
               oF43121PURLT_v := oF43121PURLT('*NONE',' ',0,0);
               pProgramLog ('FF43121PURLT', '1', 'NO DATA: Branch', oF43121PURLT_v.mcu);    
               pProgramLog ('FF43121PURLT', '1', 'NO DATA: Item Nbr', oF43121PURLT_v.litm);    
               pProgramLog ('FF43121PURLT', '1', 'NO DATA: STDDEV_POP', TO_CHAR(oF43121PURLT_v.PURLTSTDDEV));    
               pProgramLog ('FF43121PURLT', '1', 'NO DATA: AVG', TO_CHAR(oF43121PURLT_v.PURLTMEAN));    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'NO DATA: Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'NO DATA: Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'NO DATA: Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
               RETURN oF43121PURLT_v;
         WHEN OTHERS THEN
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Date Range ', TO_CHAR(datefromjul_in) || ' thru ' || TO_CHAR(datetojul_in));    
               Select LISTAGG(DCTO || ',' || LNTY || ',' || LTTR,';') WITHIN GROUP (ORDER BY DCTO) 
               INTO parmStr
               FROM TABLE(tF43121SEL_in);
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'F42199 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('FF43121PURLT', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
               raise_application_error(-20011,'Unknown Exception in FF43121PURLT Procedure');     
  END FF43121PURLT;  

    /* Calculate Total Purchase Lead-time */
  FUNCTION FF43090TotPurLT(mcu_in VARCHAR, itm_in NUMBER, LTPrepare_in IN NUMBER, LTReceive_in IN NUMBER)
    RETURN NUMBER
    AS
      calLTLV NUMBER;
    BEGIN
      SELECT greatest(ceil(median(NVL(pc.pcaltd/100,2))), ceil(avg(NVL(pc.pcaltd/100,2)))) + NVL(LTPrepare_in,0) + NVL(LTReceive_in,0)  as newLTLV
      INTO calLTLV
      FROM F43090 pc
      WHERE pc.PCITM = itm_in
      AND pc.PCMCU = mcu_in
      AND pkCPMEMAIN.updateDateJul between pc.pccefj and pc.pccxpj
      AND pc.pcaltd > 0;
      RETURN calLTLV;
  END FF43090TotPurLT;
  
   /* Parse Parameter String */
  FUNCTION fParseParameter (str_in IN VARCHAR2,delimiter_in IN VARCHAR2)
    RETURN tParsedStr
    AS
       TYPE tParsed IS TABLE OF VARCHAR(32767) INDEX BY PLS_INTEGER;
       tParsed_v tParsed;
       tParsedStr_v tParsedStr := tParsedStr();
    BEGIN
    IF delimiter_in = ',' THEN
    SELECT str
    BULK COLLECT INTO tParsed_v
      FROM (SELECT REGEXP_SUBSTR (str_in, '[^,]+', 1, LEVEL) str
           FROM   (SELECT   str_in str FROM DUAL)
           CONNECT BY   LEVEL <= regexp_count (str, ',', 1) + 1)
     WHERE   str IS NOT NULL;
     ELSE  --delimiter_in = ';' 
     SELECT str
     BULK COLLECT INTO tParsed_v
      FROM (SELECT REGEXP_SUBSTR (str_in, '[^;]+', 1, LEVEL) str
              FROM (SELECT str_in str FROM DUAL)
              CONNECT BY   LEVEL <= regexp_count (str, ';', 1) + 1)
     WHERE   str IS NOT NULL;
     END IF;
     
     FOR indx IN 1 .. tParsed_v.Count
     LOOP
     tParsedStr_v.Extend;
     tParsedStr_v(tParsedStr_v.Count) := oParsedStr(tParsed_v(indx));
     END LOOP;
    RETURN tParsedStr_v;
    END fParseParameter;

  /* Parse Selection Criteria for F42199 */
   FUNCTION FF42199SEL(parmstring_in VARCHAR2) 
   RETURN tF42199Sel
   AS
     parsedstr01_v tParsedStr;
     parsedstr02_v tParsedStr;
     tF42199Sel_v tF42199Sel := tF42199Sel();
     oF42199Sel_v oF42199Sel;
     parsedStr_in VARCHAR2(32767);
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       parsedStr_in := parsedstr01_v(z).ParsedStr;
       parsedstr02_v := fParseParameter(parsedStr_in,',');
       tF42199Sel_v.EXTEND;
       oF42199Sel_v := oF42199Sel(parsedstr02_v(1).parsedstr,parsedstr02_v(2).parsedstr,TO_CHAR(parsedstr02_v(3).parsedstr));
       tF42199Sel_v(tF42199Sel_v.count) := oF42199Sel_v;
    END LOOP;
    RETURN tF42199Sel_v;
    END FF42199SEL;

  /* Parse Selection Criteria for F43121 */
   FUNCTION FF43121SEL(parmstring_in VARCHAR2) 
   RETURN tF43121Sel
   AS
     parsedstr01_v tParsedStr;
     parsedstr02_v tParsedStr;
     tF43121Sel_v tF43121Sel := tF43121Sel();
     oF43121Sel_v oF43121Sel;
     parsedStr_in VARCHAR2(32767);
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       parsedStr_in := parsedstr01_v(z).ParsedStr;
       parsedstr02_v := fParseParameter(parsedStr_in,',');
       tF43121Sel_v.EXTEND;
       oF43121Sel_v := oF43121Sel(1,parsedstr02_v(1).parsedstr,parsedstr02_v(2).parsedstr,TO_CHAR(parsedstr02_v(3).parsedstr));
       tF43121Sel_v(tF43121Sel_v.count) := oF43121Sel_v;
    END LOOP;
    RETURN tF43121Sel_v;
    END FF43121SEL;

  /* Parse Selection Criteria for F42102 */
   FUNCTION FF4102SEL(parmstring_in VARCHAR2) 
   RETURN tF4102Sel
      AS
     parsedstr01_v tParsedStr;
     parsedstr02_v tParsedStr;
     tF4102Sel_v tF4102Sel := tF4102Sel();
     oF4102Sel_v oF4102Sel;
     parsedStr_in VARCHAR2(32767);
  BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       parsedStr_in := parsedstr01_v(z).ParsedStr;
       parsedstr02_v := fParseParameter(parsedStr_in,',');
       tF4102Sel_v.EXTEND;
       oF4102Sel_v := oF4102Sel(parsedstr02_v(1).parsedstr,parsedstr02_v(2).parsedstr);
       tF4102Sel_v(tF4102Sel_v.count) := oF4102Sel_v;
    END LOOP;
    RETURN tF4102Sel_v;
   END FF4102SEL;

  /* Parse Selection Criteria for F42102 Active Branches */
   FUNCTION FF4102MCUSEL(parmstring_in VARCHAR2) 
   RETURN tF4102MCUSel
   AS
     parsedstr01_v tParsedStr;
     tF4102MCUSel_v tF4102MCUSel := tF4102MCUSel();
     oF4102MCUSel_v oF4102MCUSel;
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       tF4102MCUSel_v.EXTEND;
       oF4102MCUSel_v := oF4102MCUSel(parsedstr01_v(z).parsedstr);
       tF4102MCUSel_v(tF4102MCUSel_v.count) := oF4102MCUSel_v;
    END LOOP;
    RETURN tF4102MCUSel_v;
   END FF4102MCUSel;
   
   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
   FUNCTION FF4102EXCLPRP2_LTLV(parmstring_in VARCHAR2) 
   RETURN tF4102CatCode3Char
      AS
     parsedstr01_v tParsedStr;
     tF4102ExclPPR2_LTLV_v tF4102CatCode3Char := tF4102CatCode3Char();
     oF4102ExclPPR2_LTLV_v oF4102CatCode3Char;
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       tF4102ExclPPR2_LTLV_v.EXTEND;
       oF4102ExclPPR2_LTLV_v := oF4102CatCode3Char(parsedstr01_v(z).parsedstr);
       tF4102ExclPPR2_LTLV_v(tF4102ExclPPR2_LTLV_v.count) := oF4102ExclPPR2_LTLV_v;
    END LOOP;
    RETURN tF4102ExclPPR2_LTLV_v;
   END FF4102EXCLPRP2_LTLV;  
   
   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
   FUNCTION FF4102EXCLPRP2_ABCS(parmstring_in VARCHAR2) 
   RETURN tF4102CatCode3Char
      AS
     parsedstr01_v tParsedStr;
     tF4102ExclPPR2_ABCS_v tF4102CatCode3Char := tF4102CatCode3Char();
     oF4102ExclPPR2_ABCS_v oF4102CatCode3Char;
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       tF4102ExclPPR2_ABCS_v.EXTEND;
       oF4102ExclPPR2_ABCS_v := oF4102CatCode3Char(parsedstr01_v(z).parsedstr);
       tF4102ExclPPR2_ABCS_v(tF4102ExclPPR2_ABCS_v.count) := oF4102ExclPPR2_ABCS_v;
    END LOOP;
    RETURN tF4102ExclPPR2_ABCS_v;
   END FF4102EXCLPRP2_ABCS;     

   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
   FUNCTION FF4102EXCLPRP2_SAFE(parmstring_in VARCHAR2) 
   RETURN tF4102CatCode3Char
    AS
     parsedstr01_v tParsedStr;
     tF4102ExclPPR2_SAFE_v tF4102CatCode3Char := tF4102CatCode3Char();
     oF4102ExclPPR2_SAFE_v oF4102CatCode3Char;
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       tF4102ExclPPR2_SAFE_v.EXTEND;
       oF4102ExclPPR2_SAFE_v := oF4102CatCode3Char(parsedstr01_v(z).parsedstr);
       tF4102ExclPPR2_SAFE_v(tF4102ExclPPR2_SAFE_v.count) := oF4102ExclPPR2_SAFE_v;
    END LOOP;
    RETURN tF4102ExclPPR2_SAFE_v;
   END FF4102EXCLPRP2_SAFE;     
   
   /* Parse Selection Criteria for F42102 ABCS Rank Service Level */
   FUNCTION FF4102SERVLVL(parmstring_in IN VARCHAR2) 
   RETURN tF4102SERVLVL   
   AS
     parsedstr01_v tParsedStr;
     parsedstr02_v tParsedStr;
     tF4102SERVLVL_v tF4102SERVLVL := tF4102SERVLVL();
     oF4102SERVLVL_v oF4102SERVLVL;
     parsedStr_in VARCHAR2(32767);
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       parsedStr_in := parsedstr01_v(z).ParsedStr;
       parsedstr02_v := fParseParameter(parsedStr_in,',');
       tF4102SERVLVL_v.EXTEND;
       oF4102SERVLVL_v := oF4102SERVLVL(parsedstr02_v(1).parsedstr,TO_NUMBER(parsedstr02_v(2).parsedstr),TO_NUMBER(parsedstr02_v(3).parsedstr));
       tF4102SERVLVL_v(tF4102SERVLVL_v.count) := oF4102SERVLVL_v;
    END LOOP;
    RETURN tF4102SERVLVL_v;
   END FF4102SERVLVL;
   
   /* Given JDE Julian Date Range, return Days DiFFerence */
   FUNCTION fDtJulDiFFDys(DtJulFrm IN NUMBER, DtJulTo IN NUMBER) 
   RETURN NUMBER
   AS
      diFFDays_v NUMBER;
   BEGIN
     diFFDays_v  := TO_DATE(DtJulTo + 1900000, 'YYYYDDD') - TO_DATE(DtJulFrm + 1900000, 'YYYYDDD') + 1;
     RETURN diFFDays_v;
   END fDtJulDiFFDys;

   /* Parse Exlusion Criteria for Foreacts based on Item B/P PRP2 field */
   FUNCTION FF4102EXCLPRP2_FORECAST(parmstring_in VARCHAR2) 
   RETURN tF4102CatCode3Char
    AS
     parsedstr01_v tParsedStr;
     tF4102ExclPPR2_FORECAST_v tF4102CatCode3Char := tF4102CatCode3Char();
     oF4102ExclPPR2_FORECAST_v oF4102CatCode3Char;
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       tF4102ExclPPR2_FORECAST_v.EXTEND;
       oF4102ExclPPR2_FORECAST_v := oF4102CatCode3Char(parsedstr01_v(z).parsedstr);
       tF4102ExclPPR2_FORECAST_v(tF4102ExclPPR2_FORECAST_v.count) := oF4102ExclPPR2_FORECAST_v;
    END LOOP;
    RETURN tF4102ExclPPR2_FORECAST_v;
   END FF4102EXCLPRP2_FORECAST;     
   
   /* Parse Selection Criteria for Monthly Foreacts for Forecast Spreading */
   FUNCTION FF3460MTHFCSEL(parmstring_in VARCHAR2) 
   RETURN tF3460MTHFCSEL
    AS
     parsedstr01_v tParsedStr;
     tF3460MTHFCSEL_v tF3460MTHFCSEL := tF3460MTHFCSEL();
     oF3460MTHFCSEL_v oF3460MTHFCSEL;
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       tF3460MTHFCSEL_v.EXTEND;
       oF3460MTHFCSEL_v := oF3460MTHFCSEL(parsedstr01_v(z).parsedstr);
       tF3460MTHFCSEL_v(tF3460MTHFCSEL_v.count) := oF3460MTHFCSEL_v;
    END LOOP;
    RETURN tF3460MTHFCSEL_v;
   END FF3460MTHFCSEL;     

   /* Parse Inclusion Criteria for Item B/P Planning Code MPST  */
   FUNCTION FF4102MPST(parmstring_in VARCHAR2) 
   RETURN tF4102MPST
    AS
     parsedstr01_v tParsedStr;
     tF4102MPST_v tF4102MPST := tF4102MPST();
     oF4102MPST_v oF4102MPST;
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       tF4102MPST_v.EXTEND;
       oF4102MPST_v := oF4102MPST(parsedstr01_v(z).parsedstr);
       tF4102MPST_v(tF4102MPST_v.count) := oF4102MPST_v;
    END LOOP;
    RETURN tF4102MPST_v;
   END FF4102MPST;

   /* Parse Forecast Monthly to Weekly Spreading Percentages */
   FUNCTION FF3460SPREADPRCNT(parmstring_in VARCHAR2) 
   RETURN tF3460SPREADPRCNT
    AS
     parsedstr01_v tParsedStr;
     tf3460SPREADPRCNT_v tf3460SPREADPRCNT := tf3460SPREADPRCNT();
     of3460SPREADPRCNT_v of3460SPREADPRCNT;
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,',');
    --FOR z IN 1..parsedstr01_v.count LOOP
       tf3460SPREADPRCNT_v.EXTEND;
       of3460SPREADPRCNT_v := of3460SPREADPRCNT(parsedstr01_v(1).parsedstr,0,0);
       IF of3460SPREADPRCNT_v.SPREADTYPE != 'SPREAD' THEN
          of3460SPREADPRCNT_v.RATIO1 := 0;
          of3460SPREADPRCNT_v.RATIO2 := 0;
       ELSE
          of3460SPREADPRCNT_v.RATIO1 := TO_NUMBER(parsedstr01_v(2).parsedstr);
          of3460SPREADPRCNT_v.RATIO2 := TO_NUMBER(parsedstr01_v(3).parsedstr);
       END IF;
       tf3460SPREADPRCNT_v(1) := of3460SPREADPRCNT_v;
    --END LOOP;
    RETURN tf3460SPREADPRCNT_v;
   END FF3460SPREADPRCNT;    
   
   /* Parse Forecast Type to use for Weekly Forecasts */
   FUNCTION FF3460WKFCSEL(parmstring_in VARCHAR2) 
   RETURN tF3460WKFCSEL
    AS
     parsedstr01_v tParsedStr;
     tF3460WKFCSEL_v tF3460WKFCSEL := tF3460WKFCSEL();
     oF3460WKFCSEL_v VARCHAR2(2);
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       tF3460WKFCSEL_v.EXTEND;
       oF3460WKFCSEL_v := parsedstr01_v(z).parsedstr;
       tF3460WKFCSEL_v(tF3460WKFCSEL_v.count) := oF3460WKFCSEL_v;
    END LOOP;
    RETURN tF3460WKFCSEL_v;
   END FF3460WKFCSEL;     

  
   /* Debugging Routine that Logs Procedure and Function logic */
   PROCEDURE pProgramLog (PROGRAM_in VARCHAR2, error_in VARCHAR2, message_in VARCHAR2, description_in CLOB)
   IS
    PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
    INSERT INTO F5534CPM (CPZ55ID, CPZ55TIME, CPZ55PID, CPZ55ERR, CPZ55MSG, CPZ55DSC,CPZ55TDAY)
    VALUES (F5534CPMSeq.NEXTVAL, TO_NUMBER(TO_CHAR(To_Date(SYSDATE, 'DD-MM-YY'),'YYYYDDD') - 1900000), PROGRAM_in, error_in, message_in, description_in,to_number(to_char(systimestamp,'HHMISS' )) );
    COMMIT;
   END pProgramLog;

   /* Value Change Logging */
   PROCEDURE pLogValChg (PROGRAM_in	IN VARCHAR2, TABLE_NAME_in	IN VARCHAR2, CHG_FIELD_in	IN VARCHAR2, CHG_NUM_VALUE_in	IN NUMBER, CHG_CHAR_VALUE_in	IN VARCHAR2, KEY1_FIELD_in	IN VARCHAR2, KEY1_NUM_VALUE_in	IN NUMBER, KEY1_CHAR_VALUE_in	IN VARCHAR2, KEY2_FIELD_in	IN VARCHAR2, KEY2_NUM_VALUE_in	IN NUMBER, KEY2_CHAR_VALUE_in	IN VARCHAR2, KEY3_FIELD_in	IN VARCHAR2, KEY3_NUM_VALUE_in	IN NUMBER, KEY3_CHAR_VALUE_in	IN VARCHAR2, OLD_CHAR_VALUE_in	IN VARCHAR2, OLD_NUM_VALUE_in	IN NUMBER)
   IS
    PRAGMA AUTONOMOUS_TRANSACTION;
   BEGIN
    INSERT INTO F5534VAL (VAZ55ID, VAZ55TIME, VAZ55PID, VAZ55TBL, VAZ55CHFIELD, VAZ55CHGNUM, VAZ55CHGVAL, VAZ55KEY1FLD, VAZ55KEY1NUM, VAZ55KEY1VAL, VAZ55KEY2FLD, VAZ55KEY2NUM, VAZ55KEY2VAL, VAZ55KEY3FLD, VAZ55KEY3NUM, VAZ55KEY3VAL, VAZ55OLDVAL, VAZ55OLDNUM,VAZ55TDAY)
    VALUES (F5534VALSeq.NEXTVAL, TO_NUMBER(TO_CHAR(To_Date(SYSDATE, 'DD-MM-YY'),'YYYYDDD') - 1900000), PROGRAM_in, TABLE_NAME_in, CHG_FIELD_in, CHG_NUM_VALUE_in, CHG_CHAR_VALUE_in, KEY1_FIELD_in, KEY1_NUM_VALUE_in, KEY1_CHAR_VALUE_in, KEY2_FIELD_in, KEY2_NUM_VALUE_in, KEY2_CHAR_VALUE_in, KEY3_FIELD_in, KEY3_NUM_VALUE_in, KEY3_CHAR_VALUE_in, OLD_CHAR_VALUE_in, OLD_NUM_VALUE_in,to_number(to_char(systimestamp,'HHMISS' )) );
    COMMIT; 
   END pLogValChg;

  /* Parse Selection Criteria for F3460 */
   FUNCTION FF3460Sel(parmstring_in VARCHAR2) 
   RETURN tF3460Sel
    AS
     parsedstr01_v tParsedStr;
     tF3460Sel_v tF3460Sel := tF3460Sel();
     oF3460Sel_v oF3460Sel;
   BEGIN
    parsedstr01_v := fParseParameter(parmstring_in,';');
    FOR z IN 1..parsedstr01_v.count LOOP
       tF3460Sel_v.EXTEND;
       oF3460Sel_v := oF3460Sel(parsedstr01_v(z).parsedstr);
       tF3460Sel_v(tF3460Sel_v.count) := oF3460Sel_v;
    END LOOP;
    RETURN tF3460Sel_v;
   END FF3460Sel;     


  FUNCTION fF3460SlsHstDemand(datefromjul_in IN NUMBER, datetojul_in IN NUMBER, tF3460Sel_in IN tF3460Sel, mcu_in IN VARCHAR2, itm_in IN NUMBER)
  RETURN oF3460SlsHstDemand
  AS
    oF3460SlsHstDemand_v oF3460SlsHstDemand; 
    parmStr VARCHAR2(1000);
  BEGIN
     SELECT oF3460SlsHstDemand(sh.MFMCU, sh.MFLITM, 
          STDDEV_POP(sh.MFFQT), AVG(sh.MFFQT)) 
            INTO oF3460SlsHstDemand_v      
            FROM  F3460 sh
            JOIN (SELECT TYPF FROM TABLE(tF3460Sel_in)) sel
              ON sh.MFTYPF = sel.TYPF
            WHERE sh.MFMCU = mcu_in 
            AND sh.MFITM = itm_in
            AND sh.MFFQT > 0
            AND sh.MFDRQJ BETWEEN datefromjul_in AND datetojul_in
            GROUP BY sh.MFMCU, sh.MFLITM ;            
      CASE
        WHEN oF3460SlsHstDemand_v.FQTSTDDEV IS NULL THEN oF3460SlsHstDemand_v.FQTSTDDEV := 0;
        WHEN oF3460SlsHstDemand_v.FQTMEAN IS NULL THEN oF3460SlsHstDemand_v.FQTMEAN := 0;
        ELSE
          RETURN oF3460SlsHstDemand_v;
        END CASE;
      RETURN oF3460SlsHstDemand_v;
       EXCEPTION
          WHEN ZERO_DIVIDE THEN
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'F3460 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'ZERO_DIVIDE - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'ZERO_DIVIDE - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'ZERO_DIVIDE - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN VALUE_ERROR THEN
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'F3460 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'VALUE_ERROR - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'VALUE_ERROR - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'VALUE_ERROR - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
          WHEN COLLECTION_IS_NULL THEN
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'F3460 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'COLLECTION_IS_NULL - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'COLLECTION_IS_NULL - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'COLLECTION_IS_NULL - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN TOO_MANY_ROWS THEN
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'F3460 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'TOO_MANY_ROWS - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'TOO_MANY_ROWS - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'TOO_MANY_ROWS - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
         WHEN NO_DATA_FOUND THEN
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'F3460 Selection Criteria', parmStr);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Branch', mcu_in);    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Nbr', TO_CHAR(itm_in));    
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'NO_DATA_FOUND - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'NO_DATA_FOUND - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
               pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'NO_DATA_FOUND - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
             RETURN oF3460SlsHstDemand_v;
          WHEN OTHERS THEN
              pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'F3460 Selection Criteria', parmStr);    
              pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Branch', mcu_in);    
              pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'Item Nbr', TO_CHAR(itm_in));    
              pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'OTHERS - Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
              pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'OTHERS - Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
              pkCPMEMAIN.pProgramLog ('fF3460SlsHstDemand', '1', 'OTHERS - Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
              raise_application_error(-20011,'Unknown Exception in fF3460SlsHstDemand Procedure');     
END fF3460SlsHstDemand;  
    
END pkCPMEMAIN;
/