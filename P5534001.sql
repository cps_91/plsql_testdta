/* Calculate Total Purchasing Leadtime and Update F4102.IBLTLV */

CREATE OR REPLACE
PROCEDURE P5534001(F4102MCUSEL_in IN VARCHAR2,F4102Sel_in IN VARCHAR2,F4102EXCLPRP2_LTLV_in IN VARCHAR2, 
      F4102MPST_in IN VARCHAR2, F4102LTLV_in IN VARCHAR2, LTPrepare_in IN VARCHAR2, LTReceive_in IN VARCHAR2)
AS
    /* Limit value specifying the number of records to process and
       update at no time with each pass through the F4102_cur cursor */
    curlimit_v NUMBER(4) := 1000;
    F4102LTLV_v NUMBER(3) := To_NUMBER(F4102LTLV_in);
    LTPrepare_v NUMBER(3) := To_NUMBER(LTPrepare_in);
    LTReceive_v NUMBER(3) := To_NUMBER(LTReceive_in);
    
    /* Section: Tables for storing parsed parameters */
     /* Parameter Selection for F4102 */
    tF4102SEL_v tF4102SEL;
     /* Parameter Selection for F4102 Active Branches*/
    tF4102MCUSEL_v tF4102MCUSEL;
     /* Parameter Selection for Excluding F4102 PRP2 Codes */
    tF4102EXCLPRP2_LTLV_v tF4102CATCODE3CHAR;
     /* Parameter Selection for Including Planning Codes */
    tF4102MPST_v tF4102MPST;
    
       /* Cursor for retrieving active Branch/Plants */
    CURSOR F4102_ActiveMCU_cur IS
      SELECT MCU 
      FROM TABLE(tF4102MCUSel_v);
 
    rF4102_ActiveMCU F4102_ActiveMCU_cur%ROWTYPE;  

    /* Cursor for selecting F4102 Records to Update */ 
    CURSOR F4102_cur(mcu_in VARCHAR2) IS
        SELECT ib.IBMCU mcu, ib.IBITM itm, ib.IBLITM litm, ib.IBSTKT stkt,
             ib.IBLNTY lnty, ib.IBLTLV oldLTLV
       FROM F4102 ib
       JOIN (SELECT STKT, LNTY FROM TABLE(tF4102SEL_v)) sl
        ON ib.IBSTKT = sl.STKT
        AND ib.IBLNTY = sl.LNTY
       JOIN (SELECT MPST FROM TABLE(tF4102MPST_v)) p1
        ON p1.MPST = ib.IBMPST
       LEFT JOIN (SELECT CatCode3 FROM TABLE(tF4102EXCLPRP2_LTLV_v)) p2
        ON ib.IBPRP2 = p2.CatCode3
       WHERE ib.IBMCU = mcu_in
       AND p2.CatCode3 IS NULL
       FOR UPDATE OF IBLTLV;

     TYPE tF4102 IS TABLE OF F4102_cur%ROWTYPE
      INDEX BY PLS_INTEGER;   
      
      tF4102_v tF4102;
  
      /* Storage variable for calculated Total Purchasing Lead-time */ 
      ltlv_v NUMBER;
  
BEGIN
    pkCPMEMAIN.pProgramLog ('P5534001', '0', 'Start', 'Calculate Total Purchase Lead-time, and Update F4102.IBLVLT');    
    pkCPMEMAIN.pProgramLog ('P5534001', '0', 'Parameter 1: Item B/P MCU Inclusions',F4102MCUSEL_in);    
    pkCPMEMAIN.pProgramLog ('P5534001', '0', 'Parameter 2: Item B/P STKT and LNTY Inclusions',F4102Sel_in);    
    pkCPMEMAIN.pProgramLog ('P5534001', '0', 'Parameter 3: Item B/P PRP2 Exclusions',F4102EXCLPRP2_LTLV_in);    
    pkCPMEMAIN.pProgramLog ('P5534001', '0', 'Parameter 4: Item B/P MPST Inclusions',F4102MPST_in);    
    pkCPMEMAIN.pProgramLog ('P5534001', '0', 'Parameter 5: Item B/P LTLV Default',F4102LTLV_in);    
    pkCPMEMAIN.pProgramLog ('P5534001', '0', 'Parameter 6: Total Lead-time Prepare Days',LTPrepare_in);    
    pkCPMEMAIN.pProgramLog ('P5534001', '0', 'Parameter 7: Total Lead-time Receive Days',LTReceive_in);    
 
    /* Populate F4102 Parameter Selection Criteria */
     tF4102SEL_v := pkCPMEMAIN.FF4102SEL(F4102Sel_in);
     
    /* Populate Active Branches Selection Criteria */
    tF4102MCUSEL_v := pkCPMEMAIN.FF4102MCUSEL(F4102MCUSEL_in);
   
   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
    tF4102EXCLPRP2_LTLV_v := pkCPMEMAIN.FF4102EXCLPRP2_LTLV(F4102EXCLPRP2_LTLV_in);

   /* Parse Selection Criteria for F42102 MPST to Include */
    tF4102MPST_v := pkCPMEMAIN.FF4102MPST(F4102MPST_in);    
   
    /* Active Item Branch/Plants */
    OPEN F4102_ActiveMCU_cur;
    LOOP
      FETCH F4102_ActiveMCU_cur INTO rF4102_ActiveMCU;
      EXIT WHEN F4102_ActiveMCU_cur%NOTFOUND; 
 
         OPEN F4102_cur(rF4102_ActiveMCU.mcu);
         LOOP
         FETCH F4102_cur BULK COLLECT INTO tF4102_v LIMIT curlimit_v;
           EXIT WHEN F4102_cur%NOTFOUND;
           EXIT WHEN tF4102_v.COUNT = 0;
               
           FOR indx IN 1 .. tF4102_v.COUNT
           LOOP
                    
                     /* Determine most conservative Total Lead-time by first
                        Checking to see if the current Lead-time Level equals 
                        Zero.  If so, then set to 3.  Next, compare the current 
                        Lead-time value to the new calculated value and take 
                        The greater of the two. */  
                        ltlv_v := GREATEST(Case When tF4102_v(indx).oldLTLV = 0 Then F4102LTLV_v  
                                  Else tF4102_v(indx).oldLTLV 
                                  End, pkCPMEMAIN.FF43090TotPurLT(tF4102_v(indx).mcu, tF4102_v(indx).itm, LTPrepare_v, LTReceive_v));
                      
                       UPDATE F4102 ib
                        SET ib.IBLTLV = ltlv_v,
                          IBPID = 'P5534001',
                          IBUSER = 'PL/SQL',
                          IBUPMJ = pkCPMEMAIN.updateDateJul,
                          IBTDAY = TO_NUMBER(TO_CHAR(CURRENT_TIMESTAMP(6),'HH24MISS'))
                       WHERE CURRENT OF F4102_cur;
                       
                       pkCPMEMAIN.pLogValChg ('P5534001','F4102','IBLTLV',ltlv_v,NULL,'IBMCU',NULL,tF4102_v(indx).mcu,'IBLITM',NULL,tF4102_v(indx).LITM,NULL,NULL,NULL, NULL,tF4102_v(indx).oldLTLV);
       
           END LOOP; --indx
 
    END LOOP; --F4102_cur
    CLOSE F4102_cur;
    COMMIT; --Commit cannot be witin F4102_cur because of the UPDATE FOR
    
  END LOOP;
  CLOSE F4102_ActiveMCU_cur;

  COMMIT; 

  pkCPMEMAIN.pProgramLog ('P5534001', '0', 'End', '');    

   EXCEPTION
      WHEN OTHERS THEN
	    CLOSE F4102_cur;      
  	  CLOSE F4102_ActiveMCU_cur;
      pkCPMEMAIN.pProgramLog ('P5534001', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
      pkCPMEMAIN.pProgramLog ('P5534001', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
      pkCPMEMAIN.pProgramLog ('P5534001', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
 
END P5534001;
/
GRANT EXECUTE ON TESTDTA.p5534001 to bssvplsql;