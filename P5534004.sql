/* Central Planning */
/* Spread Monthly Forecasts into Weekly Buckets */ 
/* and insert back into F3460  */

CREATE OR REPLACE
PROCEDURE P5534004(vDtFrmGre_in IN VARCHAR2, vDtToGre_in IN VARCHAR2, F4102MCUSEL_in IN VARCHAR2,  
                   F4102Sel_in IN VARCHAR2, F4102EXCLPRP2_FORECAST_in IN VARCHAR2, F3460MTHFCSEL_in IN VARCHAR2,
                   F3460WKFCSEL_in IN VARCHAR2, F3460SPREADPRCNT_in IN VARCHAR2)
AS
    vDtFrmJul_v NATURAL := TO_CHAR(To_Date(vDtFrmGre_in, 'DD-MM-YY'),'YYYYDDD') - 1900000;
    vDtToJul_v NATURAL := TO_CHAR(To_Date(vDtToGre_in, 'DD-MM-YY'),'YYYYDDD') - 1900000;
    curlimit_v NUMBER := 1000;
   
    /* Section: Tables for storing parsed parameters */
    /* Parameter Selection for F4102 */
    tF4102SEL_v tF4102SEL;
    /* Parameter Selection for F4102 Active Branches*/
    tF4102MCUSEL_v tF4102MCUSEL;
    /* Parameter Selection for Excluding F4102 PRP2 Codes */
    tF4102EXCLPRP2_FORECAST_v tF4102CATCODE3CHAR;
    /* Parameter Selection for Monthly Forecasts to Select*/
    tF3460MTHFCSEL_v tF3460MTHFCSEL;
    /* Parameter Selection for Weekly Forecast Spread ercentages */
    tF3460SPREADPRCNT_v tF3460SPREADPRCNT;

    /* Cursor for retrieving active Branch/Plants */
    CURSOR F4102_ActiveMCU_cur IS
      SELECT MCU 
      FROM TABLE(tF4102MCUSel_v);
 
    rF4102_ActiveMCU F4102_ActiveMCU_cur%ROWTYPE;  
    tF0007CAL_v tF0007CAL;  
    tF0007FRSTWRKDYOFWK_v tF0007FRSTWRKDYOFWK; 
    tF0007WRKDYSPERMT_v tF0007WRKDYSPERMT;  
    tF0007WKLYALLOCPERCNT_v tF0007WKLYALLOCPERCNT; 
    tF0007WKOFMTDYS_v tF0007WKOFMTDYS;
    tF0007WKSPERMT_v tF0007WKSPERMT;
    tF0007WKLYBCKTS_v tF0007WKLYBCKTS;

   /* Cursor for selecting F4102 Records to Update */ 
    CURSOR F4102_cur(mcu_in VARCHAR2) IS
        SELECT mcu_in mcu, ib.IBITM itm, ib.IBLITM litm, ib.IBSTKT stkt,
             ib.IBLNTY lnty, ib.IBSAFE oldSAFE, ib.IBABCS abcs
       FROM F4102 ib
       JOIN (SELECT STKT, LNTY FROM TABLE(tF4102SEL_v)) sl
        ON ib.IBSTKT = sl.STKT
        AND ib.IBLNTY = sl.LNTY
       LEFT JOIN (SELECT CatCode3 FROM TABLE(tF4102EXCLPRP2_Forecast_v)) p2
        ON ib.IBPRP2 = p2.CatCode3
       WHERE ib.IBMCU = mcu_in
       AND p2.CatCode3 IS NULL
       AND (
            ib.IBABCS IN ('A','B')
            OR ib.IBPRP9 = 'NEW');

     TYPE tF4102 IS TABLE OF F4102_cur%ROWTYPE
      INDEX BY PLS_INTEGER;   
      tF4102_v tF4102;

     /* Cursor for Selecting Monthly Forecasts for B/P - Item combo and generating weekly forecasts */ 
     CURSOR F3460WKLYFCS_cur(mcu_in VARCHAR2, itm_in NUMBER) IS
         SELECT  f2.MFMCU MFMCU, f2.MFITM, a.CTRY, a.YR, a.MT,  TRIM(TO_CHAR(a.CTRY)) || TRIM(TO_CHAR(a.YR,'00')) || TRIM(TO_CHAR(a.MT,'00')) MTKEY,
                  a.FRSTWRKDYOFWK MFDRQJ, 
                  f2.MFLITM, f2.MFAITM,
                  f2.MFAN8, f2.MFUORG, f2.MFAEXP, f2.MFFAM, 
                  f2.MFFQT,
                  a.WKLYALLOCPERCNT,
                  --f2.MFFQT * a.WKLYALLOCPERCNT MFFQT, 
                  F3460WKFCSEL_in MFTYPF, 
                  f2.MFDCTO, f2.MFBPFC, f2.MFRVIS, f2.MFUPMJ, f2.MFUSER, f2.MFJOBN, f2.MFPID, f2.MFYR, f2.MFTDAY, f2.MFPMPN, f2.MFPNS 
           FROM F3460 f2
           JOIN (SELECT * FROM TABLE(tF3460MTHFCSEL_v)) f3
             ON f3.FC = f2.MFTYPF
           JOIN (SELECT * FROM TABLE(tF0007WKLYALLOCPERCNT_v)) a
             ON  a.YR = TO_NUMBER(TO_CHAR(TO_DATE(f2.MFDRQJ + 1900000, 'YYYYDDD'),'YY'))
             AND a.MT = TO_NUMBER(TO_CHAR(TO_DATE(f2.MFDRQJ + 1900000, 'YYYYDDD'),'MM'))
           WHERE f2.MFMCU = mcu_in
            AND f2.MFITM = itm_in
            AND f2.MFFQT != 0
            AND f2.MFDRQJ BETWEEN vDtFrmJul_v AND vDtToJul_v
          ORDER BY  f2.MFMCU, f2.MFITM, a.CTRY, a.YR, a.MT, a.FRSTWRKDYOFWK;

     TYPE tF3460WKLYFCS IS TABLE OF F3460WKLYFCS_cur%ROWTYPE
      INDEX BY PLS_INTEGER;   
      tF3460WKLYFCS_v tF3460WKLYFCS;  
  
      parmStr CLOB;
      spreadtype_v VARCHAR2(6);
      ratio1_v NUMBER(3,2);
      ratio2_v NUMBER(3,2);
      mthlyFCRemainder_v NUMBER;
      wklyalloc_v NUMBER;
      conductUpInst_v NUMBER(1);
      totalwklyalloc_v NUMBER;
      mtkey_v VARCHAR2(5);
 
BEGIN
    /* Log Parameters Used */
    pkCPMEMAIN.pProgramLog ('P5534004', '0', 'Start', '');    
    pkCPMEMAIN.pProgramLog ('P5534004', '0', 'Parameter 1: From Date',vDtFrmGre_in);    
    pkCPMEMAIN.pProgramLog ('P5534004', '0', 'Parameter 2: To Date',vDtToGre_in);    
    pkCPMEMAIN.pProgramLog ('P5534004', '0', 'Parameter 3: Item B/P MCU Inclusions',F4102MCUSEL_in);    
    pkCPMEMAIN.pProgramLog ('P5534004', '0', 'Parameter 4: Item B/P STKT and LNTY Inclusions',F4102Sel_in);    
    pkCPMEMAIN.pProgramLog ('P5534004', '0', 'Parameter 5: Item B/P PRP2 Exclusions',F4102EXCLPRP2_FORECAST_in);    
    pkCPMEMAIN.pProgramLog ('P5534004', '0', 'Parameter 6: Monthly Forecast Types to Spreadh', F3460MTHFCSEL_in);    
    pkCPMEMAIN.pProgramLog ('P5534004', '0', 'Parameter 7: Weekly Forecast Type',F3460WKFCSEL_in);    
    pkCPMEMAIN.pProgramLog ('P5534004', '0', 'Parameter 8: Weekly Spread Type',F3460SPREADPRCNT_in);    

    /* Populate F4102 Parameter Selection Criteria */
     tF4102SEL_v := pkCPMEMAIN.FF4102SEL(F4102Sel_in);
     /* Populate Active Branches Selection Criteria */
    tF4102MCUSEL_v := pkCPMEMAIN.FF4102MCUSEL(F4102MCUSEL_in);
   /* Parse Selection Criteria for F42102 PRP2 Category Codes to Exclude */
    tF4102EXCLPRP2_FORECAST_v := pkCPMEMAIN.FF4102EXCLPRP2_FORECAST(F4102EXCLPRP2_FORECAST_in);    
   /* Parse Selection Criteria for F3460 Monthly Forecast Types to Select  */
    tF3460MTHFCSEL_v := pkCPMEMAIN.FF3460MTHFCSEL(F3460MTHFCSEL_in);    
   /* Parse Spreading Percentages */
    tF3460SPREADPRCNT_v := pkCPMEMAIN.FF3460SPREADPRCNT(F3460SPREADPRCNT_in);    

    IF tF3460SPREADPRCNT_v(1).SPREADTYPE IN ('DAYS','WEEKS') THEN
       spreadtype_v := tF3460SPREADPRCNT_v(1).SPREADTYPE;
    ELSE
       spreadtype_v := 'RATIO';
       ratio1_v := tF3460SPREADPRCNT_v(1).RATIO1;
       ratio2_v := tF3460SPREADPRCNT_v(1).RATIO2;
    END IF;
    
    /* Active Item Branch/Plants */
    OPEN F4102_ActiveMCU_cur;
    LOOP
      FETCH F4102_ActiveMCU_cur INTO rF4102_ActiveMCU;
      EXIT WHEN F4102_ActiveMCU_cur%NOTFOUND; 
 
        /* Retrieve JDE Shop Floor Calendar and Calculate Dates - Generates Daily Buckets */ 
        SELECT oF0007CAL(CZMMCU, CZCTRY, CZYR, CZMT,  
        TO_CHAR(To_Date(TO_DATE(TO_CHAR(CZMT)||'/'||TO_CHAR(CSZTD)||'/'||TO_CHAR(CZYR),'MM/DD/YY'), 'DD-MM-YY'),'YYYYDDD') - 1900000,
        TO_CHAR(To_Date(TRUNC(TO_DATE(TO_CHAR(CZMT)||'/'||TO_CHAR(CSZTD)||'/'||TO_CHAR(CZYR),'MM/DD/YY'), 'MM'), 'DD-MM-YY'),'YYYYDDD') - 1900000, 
        TO_CHAR(LAST_DAY(To_Date(TRUNC(TO_DATE(TO_CHAR(CZMT)||'/'||TO_CHAR(CSZTD)||'/'||TO_CHAR(CZYR),'MM/DD/YY'), 'MM'), 'DD-MM-YY')),'YYYYDDD') - 1900000, 
        TO_NUMBER(TO_CHAR(TO_DATE(TO_CHAR(CZMT)||'/'||TO_CHAR(CSZTD)||'/'||TO_CHAR(CZYR),'MM/DD/YY'),'IW')),
        TO_NUMBER(TO_CHAR(TO_DATE(TO_CHAR(CZMT)||'/'||TO_CHAR(CSZTD)||'/'||TO_CHAR(CZYR),'MM/DD/YY'),'W')), 
        TO_CHAR(To_Date(TRUNC(TO_DATE(TO_CHAR(CZMT)||'/'||TO_CHAR(CSZTD)||'/'||TO_CHAR(CZYR),'MM/DD/YY'), 'iw'), 'DD-MM-YY'),'YYYYDDD') - 1900000, 
        TO_CHAR(To_Date(TRUNC(TO_DATE(TO_CHAR(CZMT)||'/'||TO_CHAR(CSZTD)||'/'||TO_CHAR(CZYR),'MM/DD/YY'), 'iw') + 6, 'DD-MM-YY'),'YYYYDDD') - 1900000, 
        CSZTD)
        BULK COLLECT INTO tF0007CAL_V
        FROM F0007
        UNPIVOT (WorkDay FOR CSZTD IN (CZTD01 AS 1, CZTD02 AS 2, CZTD03 AS 3,
        CZTD04 AS 4, CZTD05 AS 5, CZTD06 AS 6,
        CZTD07 AS 7, CZTD08 AS 8, CZTD09 AS 9,
        CZTD10 AS 10, CZTD11 AS 11, CZTD12 AS 12,
        CZTD13 AS 13, CZTD14 AS 14, CZTD15 AS 15,
        CZTD16 AS 16, CZTD17 AS 17, CZTD18 AS 18,
        CZTD19 AS 19, CZTD20 AS 20, CZTD21 AS 21,
        CZTD22 AS 22, CZTD23 AS 23, CZTD24 AS 24,
        CZTD25 AS 25, CZTD26 AS 26, CZTD27 AS 27,
        CZTD28 AS 28, CZTD29 AS 29, CZTD30 AS 30, CZTD31 AS 31))
        WHERE CZMMCU = rF4102_ActiveMCU.mcu
        AND CZCTRY = TO_NUMBER(SUBSTR(vDtFrmJul_v,1,1))
        AND CZYR BETWEEN TO_NUMBER(SUBSTR(vDtFrmJul_v,2,2)) - 1 AND  TO_NUMBER(SUBSTR(vDtToJul_v,2,2))
        AND WORKDAY = 'W';

       /*  Generates Weekly Buckets and Work Days for Week of  Month */
        SELECT oF0007WKLYBCKTS(c.MCU, c.CTRY, c.YR, c.MT, c.WKOFYR, COUNT(C.WRKDY))
        BULK COLLECT INTO tF0007WKLYBCKTS_v
        FROM (SELECT * FROM TABLE(tF0007CAL_v)) c
        GROUP BY c.MCU, c.CTRY, c.YR, c.MT, c.WKOFYR
        ORDER BY c.MCU, c.CTRY, c.YR, c.MT, c.WKOFYR;
    
        /* Find the first workday of each Week of the Year.
           Will use to compare the input Forecast Date to the Date Range.
           Generates Weekly Buckets */
        SELECT oF0007FRSTWRKDYOFWK(c.MCU, c.CTRY, c.YR, c.WKOFYR, c.FRSTDYOFWK, c.LSTDYOFWK, MIN(c.WRKDY))
        BULK COLLECT INTO tF0007FRSTWRKDYOFWK_v
        FROM (SELECT * FROM TABLE(tF0007CAL_v)) c
        GROUP BY c.MCU, c.CTRY, c.YR, c.WKOFYR, c.FRSTDYOFWK, c.LSTDYOFWK
        ORDER BY c.MCU, c.CTRY, c.YR, c.WKOFYR, c.FRSTDYOFWK, c.LSTDYOFWK;
 
       /* Allocate Precentage of Monthly Forecast Quantity to Each Week for each Month */
       SELECT  oF0007WKLYALLOCPERCNT(f.MCU, f.CTRY, f.YR, c.MT, f.FRSTDYOFWK, f.LSTDYOFWK, f.FRSTWRKDYOFWK, 
                          CASE
                            /* Divide days in week by number of weeks in month */
                             WHEN  spreadtype_v  = 'DAYS' THEN c.WRKDYSINWK /   
                                                                (SELECT COUNT(c3.WKOFMT)
                                                                FROM (SELECT * FROM TABLE(tF0007CAL_v)) c3
                                                                WHERE f.MCU = c3.MCU
                                                                AND f.CTRY = c3.CTRY
                                                                AND f.YR = c3.YR
                                                                AND c.MT = c3.MT)

                             /* Divide 1 by number of weeks in month */  
                            -- WHEN  spreadtype_v  = 'WEEKS' THEN 1/ WKSPRMT (c.MCU, c.CTRY, c.YR, c.MT)
                             ELSE 1/  (SELECT COUNT(c3.WKOFMT)
                                                                FROM (SELECT * FROM TABLE(tF0007CAL_v)) c3
                                                                WHERE f.MCU = c3.MCU
                                                                AND f.CTRY = c3.CTRY
                                                                AND f.YR = c3.YR
                                                                AND c.MT = c3.MT)
                                                          
--                             ELSE
--                                 CASE WHEN  RATIO
--                                   /* Divide weeks in month by 2 and Round. For month week# <= calc, apply %1,
--                                      else if week# > calc, then apply %2 */
--                                     ROUND(WKSPRMT (c.MCU, c.CTRY, c.YR, c.MT)/2) <= c.WKOFMT THEN  ratio1_v/ROUND(WKSPRMT (c.MCU, c.CTRY, c.YR, c.MT)/2)
--                                 ELSE
--                                     ratio2_v/(WKSPRMT (c.MCU, c.CTRY, c.YR, c.MT) - ROUND(WKSPRMT (c.MCU, c.CTRY, c.YR, c.MT)/2))
--                                 END 
                             END
                          )
        BULK COLLECT INTO tF0007WKLYALLOCPERCNT_v
        FROM (SELECT * FROM TABLE(tF0007FRSTWRKDYOFWK_v)) f
        JOIN (SELECT * FROM TABLE(tF0007WKLYBCKTS_v)) c
          ON c.MCU = f.MCU
          AND c.CTRY = f.CTRY
          AND c.YR = f.YR
          AND c.WKOFYR = f.WKOFYR;
 
         OPEN F4102_cur(rF4102_ActiveMCU.mcu);
         LOOP
         FETCH F4102_cur BULK COLLECT INTO tF4102_v LIMIT curlimit_v;
           EXIT WHEN F4102_cur%NOTFOUND; 
           EXIT WHEN tF4102_v.COUNT = 0;
               
           FOR indx IN 1 .. tF4102_v.COUNT
           LOOP
 
              OPEN F3460WKLYFCS_cur(rF4102_ActiveMCU.mcu, tF4102_v(indx).ITM);
              LOOP
              FETCH F3460WKLYFCS_cur BULK COLLECT INTO tF3460WKLYFCS_v;
                --EXIT WHEN F3460WKLYFCS_cur%NOTFOUND;
                EXIT WHEN tF3460WKLYFCS_v.COUNT = 0;

                 mthlyFCRemainder_v := 0;
                 mtkey_v := '00000';
                 
                 FOR indx2 IN 1 .. tF3460WKLYFCS_v.COUNT
                 LOOP
                    EXIT WHEN tF3460WKLYFCS_v.COUNT = 0;
                   
                   /* For each new Month, reset the Forecast Remainder variable */
                   IF mtkey_v != tF3460WKLYFCS_v(indx2).MTKEY THEN
                    BEGIN
                       mthlyFCRemainder_v := tF3460WKLYFCS_v(indx2).MFFQT;
                       mtkey_v := tF3460WKLYFCS_v(indx2).MTKEY;
                    END;
                   END IF;
                   
                  IF mthlyFCRemainder_v <= 0 THEN 
                    BEGIN
                      conductUpInst_v := 0;
                      CONTINUE; --Conduct next Loop because Forecast has been completely allocated
                    END;
                  ELSE
                      conductUpInst_v := 1;
                  END IF;
                  
                  /* This section calculates the weekly allocation forecast based on the monthly forecast
                     and ensures that the total allocated is equal to the monthly foreacast */
                   wklyalloc_v := ROUND(tF3460WKLYFCS_v(indx2).MFFQT * tF3460WKLYFCS_v(indx2).WKLYALLOCPERCNT,0);

                   IF wklyalloc_v > mthlyFCRemainder_v THEN 
                      wklyalloc_v := mthlyFCRemainder_v; --If weekly allocation is greater than remaining unallocated foreacast, then set weekly alloc to remaninder
                   END IF;
                   
                   /* Update or Insert the Weekly Forecast based on the First Workday of a Week */
                   IF conductUpInst_v = 1 THEN
                    BEGIN
                    UPDATE F3460 
                      SET MFFQT = MFFQT + wklyalloc_v
                          WHERE MFMCU = tF3460WKLYFCS_v(indx2).MFMCU
                            AND MFITM = tF3460WKLYFCS_v(indx2).MFITM
                            AND MFDRQJ = tF3460WKLYFCS_v(indx2).MFDRQJ
                            AND MFAN8 = tF3460WKLYFCS_v(indx2).MFAN8
                            AND MFTYPF = tF3460WKLYFCS_v(indx2).MFTYPF;
                      IF ( sql%notfound ) THEN
                           INSERT INTO F3460 (MFITM, MFLITM, MFAITM, MFMCU, MFDRQJ, MFAN8, MFUORG, MFAEXP, MFFAM, MFFQT, MFTYPF, 
                                     MFDCTO, MFBPFC, MFRVIS, MFUPMJ, MFUSER, MFJOBN, MFPID, MFYR, MFTDAY, MFPMPN, MFPNS)
                            VALUES (tF3460WKLYFCS_v(indx2).MFITM, tF3460WKLYFCS_v(indx2).MFLITM, tF3460WKLYFCS_v(indx2).MFAITM, tF3460WKLYFCS_v(indx2).MFMCU, tF3460WKLYFCS_v(indx2).MFDRQJ, tF3460WKLYFCS_v(indx2).MFAN8, tF3460WKLYFCS_v(indx2).MFUORG, tF3460WKLYFCS_v(indx2).MFAEXP, tF3460WKLYFCS_v(indx2).MFFAM, wklyalloc_v, tF3460WKLYFCS_v(indx2).MFTYPF, 
                                     tF3460WKLYFCS_v(indx2).MFDCTO, tF3460WKLYFCS_v(indx2).MFBPFC, tF3460WKLYFCS_v(indx2).MFRVIS, tF3460WKLYFCS_v(indx2).MFUPMJ, tF3460WKLYFCS_v(indx2).MFUSER, tF3460WKLYFCS_v(indx2).MFJOBN, tF3460WKLYFCS_v(indx2).MFPID, tF3460WKLYFCS_v(indx2).MFYR, tF3460WKLYFCS_v(indx2).MFTDAY, tF3460WKLYFCS_v(indx2).MFPMPN, tF3460WKLYFCS_v(indx2).MFPNS);
                      END IF;
                      
                      mthlyFCRemainder_v := mthlyFCRemainder_v - wklyalloc_v;
                     END; 
                   END IF;
                   
                 END LOOP;   
                 COMMIT;

            END LOOP;  --F3460WKLYFCS_cur
            CLOSE F3460WKLYFCS_cur;
            COMMIT;
            
          END LOOP; --indx
 
    END LOOP; --F4102_cur
    CLOSE F4102_cur;
    COMMIT; 
    
  END LOOP;
  CLOSE F4102_ActiveMCU_cur;

  COMMIT; 
  pkCPMEMAIN.pProgramLog ('P5534004', '0', 'End',''); 

   EXCEPTION
       WHEN ZERO_DIVIDE THEN
            CLOSE F3460WKLYFCS_cur;
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN DUP_VAL_ON_INDEX THEN
            CLOSE F3460WKLYFCS_cur;
      	    CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN VALUE_ERROR THEN
            CLOSE F3460WKLYFCS_cur;
	          CLOSE F4102_cur;      
      	    CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call to FF42199Demand | Date Range ', TO_CHAR(vDtFrmJul_v) || ' thru ' || TO_CHAR(vDtToJul_v));    
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call to FF42199Demand | F42199 Selection Criteria', parmStr);    
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call to FF42199Demand | Item Branch', TO_CHAR(rF4102_ActiveMCU.mcu));    
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN SUBSCRIPT_BEYOND_COUNT THEN
            CLOSE F3460WKLYFCS_cur;
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN SUBSCRIPT_OUTSIDE_LIMIT THEN
            CLOSE F3460WKLYFCS_cur;
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN COLLECTION_IS_NULL THEN
            CLOSE F3460WKLYFCS_cur;
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN TOO_MANY_ROWS THEN
            CLOSE F3460WKLYFCS_cur;
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN NO_DATA_FOUND THEN
            CLOSE F3460WKLYFCS_cur;
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call to FF42199Demand | Date Range ', TO_CHAR(vDtFrmJul_v) || ' thru ' || TO_CHAR(vDtToJul_v));    
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call to FF42199Demand | F42199 Selection Criteria', parmStr);    
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call to FF42199Demand | Item Branch', TO_CHAR(rF4102_ActiveMCU.mcu));    
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
       WHEN OTHERS THEN
            CLOSE F3460WKLYFCS_cur;
	          CLOSE F4102_cur;      
  	        CLOSE F4102_ActiveMCU_cur;
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Call Stack',DBMS_UTILITY.FORMAT_CALL_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error Stack',DBMS_UTILITY.FORMAT_ERROR_STACK);   
            pkCPMEMAIN.pProgramLog ('P5534004', '1', 'Error backtrace',DBMS_UTILITY.FORMAT_ERROR_BACKTRACE);   
            raise_application_error(-20011,'Unknown Exception in P5534004 Procedure'); 
END P5534004;
/
GRANT EXECUTE ON TESTDTA.p5534004 to bssvplsql;